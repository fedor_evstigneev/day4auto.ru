<?php
	$langs = new LanguageModel();
	$currency = new CurrencyModel();
	$lang = $langs->SelectLanguage();
    $userCounter = new UserCounterModel();
?>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Day4shop.ru</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes" />

<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>assets/css/style.css" />
<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>assets/css/1200px.css" />
<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH; ?>../assets/css/style.css" />
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css?v=1"/>

<!-- Jquery cdn start -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<!-- Bootstrrap cdn start -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<!-- Bootstrrap cdn end -->

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.bootstrap2.min.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/js/standalone/selectize.min.js"></script>
<script src="<?= ROOT_URL; ?>assets/js/selectize.js"></script>

<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>
        tinymce.init({
            selector: 'textarea',
            height: 200,
            theme: 'modern',
            fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
            ],
            toolbar1: 'undo redo | fontsizeselect | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
            image_advtab: true,
            templates: [{
                title: 'Test template 1',
                content: 'Test 1'
            },
                {
                    title: 'Test template 2',
                    content: 'Test 2'
                }
            ],
            content_css: [
                '<?php echo ROOT_PATH; ?>assets/css/style.css',
                '//www.tinymce.com/css/codepen.min.css'
            ],
            verify_html: false,
            mode : "textareas",
            force_br_newlines : false,
            force_p_newlines : false,
            forced_root_block : ''
        });
    </script>
</head>
<body>
<?php
//    session_destroy();
    if(!isset($_SESSION['admin_logged_in'])) {
?>
        <div class="container m-t-10">
            <?php Messages::display(); ?>
            <form action="" method="post" class="adminPanelForm">
                <span class="adminPanelSpan">Admin panel</span>
                <div class="form-group">
                    <label for="email"><?php echo $lang['EMAIL']; ?>:</label>
                    <input type="text" name="mail" id="email" class="form-control" />
                </div>
                <div class="form-group">
                    <label for="password"><?php echo $lang['PASSWORD']; ?>:</label>
                    <input type="password" name="password" id="password" class="form-control" />
                </div>
                <div class="form-group">
                    <input type="submit" name="login" class="btn btn-success" value="<?php echo $lang['LOGIN']; ?>"/>
                </div>
            </form>
        </div>

<?php
    }else{
?>

<div class="test-div">
<pre class="test-pre">
<?php print_r($_SESSION); ?>
</pre>
</div>

        <nav class="navbar navbar-expand-lg bg-orange">
            <div class="container">
                <a class="navbar-brand my-brand" href="<?php echo ROOT_PATH; ?>"><img
                            src="<?php echo ROOT_URL; ?>../assets/images/day4auto_logo.svg" alt="market logo" height="35"/></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="fa fa-bars"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="<?php echo ROOT_URL; ?>">Автомобили</a>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle nav-link" data-toggle="dropdown" href="#">Детали Авто
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li class="nav-item active">
                                    <a class="nav-link" href="<?php echo ROOT_URL.'car/accessories'; ?>">Аксесуары</a>
                                </li>
                                <li class="nav-item active">
                                    <a class="nav-link" href="<?php echo ROOT_URL.'car/category/'; ?>">Категории</a>
                                </li>
                                <li class="nav-item active">
                                    <a class="nav-link" href="<?php echo ROOT_URL.'car/petrol/'; ?>">Топливо матора</a>
                                </li>
                                <li class="nav-item active">
                                    <a class="nav-link" href="<?php echo ROOT_URL.'car/gearbox/'; ?>">Коробка передач</a>
                                </li>
                                <li class="nav-item active">
                                    <a class="nav-link" href="<?php echo ROOT_URL.'car/run_type/'; ?>">Тип пробега</a>
                                </li>

                                <li class="nav-item active">
                                    <a class="nav-link" href="<?php echo ROOT_URL.'car/airbag/'; ?>">Подушки безопасности</a>
                                </li>
                                <li class="nav-item active">
                                    <a class="nav-link" href="<?php echo ROOT_URL.'car/size/'; ?>">Размер матора</a>
                                </li>

                                <li class="nav-item active">
                                    <a class="nav-link" href="<?php echo ROOT_URL.'car/rudder/'; ?>">Расположение руля</a>
                                </li>

                                <li class="nav-item active">
                                    <a class="nav-link" href="<?php echo ROOT_URL.'car/cylinder/'; ?>">Целиндры</a>
                                </li>
                                <li class="nav-item active">
                                    <a class="nav-link" href="<?php echo ROOT_URL.'car/door/'; ?>">Дверей</a>
                                </li>


                            </ul>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="<?php echo ROOT_URL; ?>terms/">Правила</a>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle nav-link" data-toggle="dropdown" href="#">Доступ
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="<?php echo ROOT_URL; ?>account/admins/" class="nav-link">
                                        Администраторы <span class="alert alert-success btn-sm"><?php echo $userCounter->Counter('u0555183_merged.admin', ''); ?></span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo ROOT_URL; ?>account/users/" class="nav-link">
                                        Пользователи <span class="alert alert-warning btn-sm"><?php echo $userCounter->Counter('u0555183_merged.users', ''); ?></span> <span class="alert alert-success btn-sm"><?php echo $userCounter->Counter('u0555183_merged.users', 'WHERE block = 0'); ?></span> <span class="alert alert-danger btn-sm"><?php echo $userCounter->Counter('u0555183_merged.users', 'WHERE block = 1'); ?></span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="<?php echo ROOT_URL.'lang'; ?>">Языки</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="<?php echo ROOT_URL.'words'; ?>">Слова</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="<?php echo ROOT_URL.'account/logout'; ?>">Выход</a>
                        </li>

                    </ul>
                </div>
            </div>
        </nav>

<div class="wrapper m-t-20">
    <div class="container">
        <?php Messages::display(); ?>
        <?php  require($view); ?>
    </div>
</div>
<?php
    }
?>



</body>
</html>