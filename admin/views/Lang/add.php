<form action="" method="post" class="border-1-ccc p-10 border-radius-5" enctype="multipart/form-data">
	<div class="form-group">
		<label for="image-input">Фотография:</label>
		<input type="file" name="image[]" id="image-input" multiple="multiple"><br/>
	</div>
	<div class="form-group">
		<label for="display_name">Визуальное Имя:</label>
		<input type="text" name="display_name" class="form-control" id="display_name" placeholder="Russian" required/>
	</div>
	
	<div class="form-group">
		<label for="determinant">Определитель:</label>
		<input type="text" name="determinant" class="form-control" id="determinant" placeholder="ru" required/>
	</div>
	
	<div class="form-group">
		<label for="culture_name">Название культуры:</label>
		<input type="text" name="culture_name" class="form-control" id="culture_name" placeholder="ru-RU" required/>
	</div>
	<div class="form-group">
		<input type="submit" name="save" value="Добавить" class="btn btn-success"/>
	</div>
</form>