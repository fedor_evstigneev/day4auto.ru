<div class="row">
    <div class="col-6 col-md-3 col-lg-2 m-t-10 cursor-pointer">
        <div class="lang-div btn-info my-btn-add-lang">
            <a href="<?php echo ROOT_URL.'lang/add/'; ?>">
                <i class="fa fa-plus fa-5x" aria-hidden="true"></i>
            </a>
        </div>
    </div>
<?php
	foreach($viewmodel as $item){
?>
        <div class="col-6 col-md-3 col-lg-2 m-t-10">
            <div class="lang-div">
                <img src="<?php echo SELECT_SHARED_IMAGE.'flag/'.$item['img']; ?>" height="50px;" />
                <p>ID:<?php echo $item['id']; ?></p>
                <p><?php echo $item['display_name']; ?></p>
                <p><?php echo $item['culture_name']; ?></p>
                <p><?php echo $item['determinant']; ?></p>
                <a href="<?php echo ROOT_URL.'lang/del/'.$item['id']; ?>" class="btn btn-danger" onclick="return confirm('Вы уверины что хотите удалить этот язык ?');">Удалить</a>
            </div>
        </div>
<?php
	}
?>
</div>