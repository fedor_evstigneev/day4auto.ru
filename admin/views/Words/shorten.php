<?php
	$wordsModel = new WordsModel();

	foreach($viewmodel as $item){
		$arrIds[] = $item['langID'];
	}

?>

<form action="" method="post" class="edit-users-form">
	<div class="form-group">
		<select name="langID" class="form-control" required>
			<option value="">Язык</option>
			<?php
				foreach($wordsModel->translateLang() as $lang){
			?>
					<option value="<?php echo $lang['id']; ?>" <?php if(in_array($lang['id'], $arrIds)){echo 'disabled';} ?> ><?php echo $lang['display_name']; ?></option>
			<?php
				}
			?>
		</select>
	</div>
	<div class="form-group">
		<label for="Trans">Перевод (Trans):</label>
		<input type="text" class="form-control" name="Trans" id="Trans" required>
	</div>
		<input type="submit" class="btn btn-success" name="save" value="сохранить" />
</form>
<br/>
<table class="table table-bordered table-striped">
	<tr>
		<th>ID</th>
		<th>shorten</th>
		<th>Слова (Trans)</th>
		<th colspan="2">Действия</th>
	</tr>
<?php 
	foreach($viewmodel as $item){
?>
		<tr>
			<td><?php echo $item['id']; ?></td>
			<td><?php echo $item['shorten']; ?></td>
			<td><?php echo $item['Trans']; ?></td>
			<td><a href="<?php echo ROOT_URL.'words/edit/'.$_GET['id'].'~'.$item['langID']; ?>" class="btn btn-info btn-sm">Изменить</a></td>
		</tr>
<?php	
	}
?>
</table>