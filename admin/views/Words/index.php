<form action="" method="post" class="filter-left-form">
    <div class="form-group">
        <label for="shorten">SHORTEN Большими Буквами ENG</label>
        <input type="text" name="shorten" class="form-control" id="shorten" required />
    </div>
    <div class="form-group">
        <label for="trans">На русском !</label>
        <input type="text" name="trans" class="form-control" id="trans" required />
    </div>
    <div class="form-group">
        <input type="submit" name="add-trans" value="Добавить перевод" class="btn btn-success" />
    </div>
</form>

<form action="" method="post" class="filter-right-form min-height-248">
    <div class="form-group">
        <label for="text">Найти слово по ID или Shorten или Trans-у</label>
        <input type="text" name="text" class="form-control" id="text" required />
    </div>
    <div class="form-group">
        <input type="submit" name="search" value="Поиск" class="btn btn-success" />
    </div>
</form>


<div class="clear"></div>

<div class="container">
    <div class="row border-1-ccc">
        <div class="col-1"><b>ID</b></div>
        <div class="col-3"><b>shorten</b></div>
        <div class="col-4"><b>Слова (Trans)</b></div>
        <div class="col-4"><b>Действие</b></div>
        <div class="col-12">
            <hr/>
        </div>
    <?php
        foreach($viewmodel as $item){
    ?>
            <div class="col-1"><?php echo $item['id']; ?></div>
            <div class="col-3 overflow-hidden"><?php echo $item['shorten']; ?></div>
            <div class="col-4 overflow-hidden"><?php echo $item['Trans']; ?></div>
            <div class="col-4"><a href="<?php echo ROOT_URL.'words/shorten/'.$item['shorten']; ?>" class="btn btn-info btn-sm">Перевести</a></div>
            <div class="col-12">
                <hr/>
            </div>
    <?php
        }
    ?>
    </div>
</div>