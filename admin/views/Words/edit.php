<form action method="post">
	<div class="form-group">
		<label for="shorten">Индикатор распознования слова</label>
		<input type="text" class="form-control" id="shorten" placeholder="Перевод" value="<?php echo $viewmodel['shorten']; ?>" disabled />
	</div>
	<div class="form-group">
		<label for="trans">Перевод</label>
		<input type="text" class="form-control" name="trans" id="trans" value="<?php echo $viewmodel['Trans']; ?>"/>
	</div>
	<div class="form-group">
		<input type="submit" name="save" value="Сохранить" class="btn btn-success" />
	</div>

</form>