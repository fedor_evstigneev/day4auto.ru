<form action="" method="post" class="border-1-ccc border-radius-5 p-10 p-b-0">
    <div class="row">
        <div class="col-9">
            <div class="form-group">
                <input type="text" name="name" placeholder="Название" class="form-control"/>
            </div>
        </div>
        <div class="col-3">
            <input type="submit" name="add" value="Добавить" class="btn btn-success float-right" />
        </div>
    </div>
</form>

<table class="table table-bordered m-t-10">
    <tr>
        <th>ID</th>
        <th>Shorten</th>
        <th>Название</th>
        <th>Изменить | Перевести | Удалить</th>
    </tr>
    <?php
    if(isset($viewmodel) && !empty($viewmodel)) {
        foreach ($viewmodel as $item) {
            ?>
            <tr>
                <td><?= $item['id']; ?></td>
                <td><?= $item['name']; ?></td>
                <td><?= $lang[$item['name']]; ?></td>
                <td>
                    <a href="<?php echo ROOT_URL.'car/'.$_GET['action'].'_edit/'.$item['id']; ?>" class="btn btn-warning btn-sm">Изменить</a>
                    <a href="<?php echo ROOT_URL.'car/'.$_GET['action'].'_translate/'.$item['id']; ?>" class="btn btn-primary btn-sm">Перевести</a>
                    <a href="<?php echo ROOT_URL.'car/'.$_GET['action'].'_delete/'.$item['id']; ?>" onclick="return confirm('Вы уверины что хотите удалить <?= $lang[$item['name']]; ?> этот аксесуар ?');" class="btn btn-danger btn-sm">Удалить</a>
                </td>
            </tr>
            <?php
        }
    }
    ?>
</table>