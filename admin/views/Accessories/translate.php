<?php
$wordsModel = new AccessoriesModel();

foreach($viewmodel as $item){
    $arrIds[] = $item['langID'];
}
?>

<form action="" method="post">
    <div class="row">
        <div class="col-sm-2">
            <div class="form-group">
                <label for="langID">Язык</label>
                <select name="langID" class="form-control" id="langID" required>
                    <option value="">Язык</option>
                    <?php
                    foreach($wordsModel->translateLang() as $lang){
                        ?>
                        <option value="<?php echo $lang['id']; ?>" <?php if(in_array($lang['id'], $arrIds)){echo 'disabled';} ?> ><?php echo $lang['display_name']; ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="col-sm-7">
            <div class="form-group">
                <label for="Trans">Перевод: <span class="alert alert-warning btn-sm"><?= $viewmodel[0]['Trans']; ?></span></label>
                <input type="text" class="form-control" name="Trans" id="Trans" required>
            </div>
        </div>
        <div class="col-sm-3">
            <label for="save">&nbsp;</label>
            <input type="submit" class="btn btn-success form-control" value="Сохранить" name="save" id="save">
        </div>
    </div>
</form>