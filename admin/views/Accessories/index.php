<a href="<?php echo ROOT_URL.'accessories/add/'; ?>" class="btn btn-success">Добавить Новый аксесуар</a>

<table class="table table-bordered m-t-10">
    <tr>
        <th>ID</th>
        <th>Название</th>
        <th>Изменить | Перевести | Удалить</th>
    </tr>
    <?php
    if(isset($viewmodel) && !empty($viewmodel)) {
        foreach ($viewmodel as $item) {
            ?>
            <tr>
                <td><?= $item['id']; ?></td>
                <td><?= $lang[$item['name']]; ?></td>
                <td>
                    <a href="<?php echo ROOT_URL.'accessories/edit/'.$item['id']; ?>" class="btn btn-warning btn-sm">Изменить</a>
                    <a href="<?php echo ROOT_URL.'accessories/translate/'.$item['id']; ?>" class="btn btn-primary btn-sm">Перевести</a>
                    <a href="<?php echo ROOT_URL.'accessories/delete/'.$item['id']; ?>" onclick="return confirm('Вы уверины что хотите удалить <?= $lang[$item['name']]; ?> этот аксесуар ?');" class="btn btn-danger btn-sm">Удалить</a>
                </td>
            </tr>
            <?php
        }
    }
    ?>
</table>