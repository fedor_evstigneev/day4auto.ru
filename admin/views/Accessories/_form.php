<form action="" method="post">
    <div class="row">
        <div class="col-9">
            <div class="form-group">
                <label for="name">Название на русском</label>
                <input type="text" name="name" class="form-control" id="name" value="<?php echo ($viewmodel['name'])? $lang[$viewmodel['name']] : '' ?>" required/>
            </div>
        </div>
        <div class="col-3">
            <label for="save">&nbsp;</label>
            <input type="submit" class="btn btn-success form-control" value="Сохранить" name="save" id="save">
        </div>
    </div>
</form>