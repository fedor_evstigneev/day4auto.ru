<?php
//echo '<pre>';
//print_r($viewmodel);
//echo '</pre>';
?>

<?php
if (!empty($viewmodel)) {
    foreach ($viewmodel as $item) {
        $firstImg = explode(',', $item['images']);
        $imageUrl = dirname(__DIR__) . '/../../assets/images/cars/mini/' . $firstImg[0];
        $image = '';
        $prodImgClass = '';

        if (!empty($firstImg[0]) && file_exists($imageUrl)) {
            $getsize = getimagesize(ROOT_SITE_URL . 'assets/images/cars/mini/' . $firstImg[0]);
            $image = ROOT_SITE_URL . 'assets/images/cars/mini/' . $firstImg[0];
            ($getsize[0] < $getsize[1]) ? $prodImgClass = 'prodImageHeight' : $prodImgClass = 'prodImageWidth';
        } else {

        }

        if($item['on_off'] == 'on'){
            $classBtn = 'btn-success';
            $textBtn = 'Включено';
            $linkBtn = 'off';
        }else{
            $classBtn = 'btn-secondary';
            $textBtn = 'Отключено';
            $linkBtn = 'on';
        }

        ?>
        <div itemscope itemtype="http://schema.org/Product" class="products">
            <div class="row">
                <div class="col-12 m-b-5">
                    <div class="btn-group btn-group-xs">
                        <a href="<?php echo ROOT_URL.'home/'.$linkBtn.'/'.$item['id'].'/' ?>" class="btn <?= $classBtn ?> btn-sm"><?= $textBtn ?></a>
                        <a href="<?php echo ROOT_URL.'home/delete/'.$item['id']; ?>" class="btn btn-danger btn-sm" onclick="return confirm('Вы уверины что хотите удалить это объявление?');">Удалить</a>
                        <a href="<?php echo ROOT_URL.'home/edit/'.$item['id']; ?>" class="btn btn-warning btn-sm">Изменить</a>
                        <a href="<?php echo ROOT_URL.'home/vip/'.$item['id']; ?>" onclick="return confirm('Цена Vip-а составляет 35 Рублей');" class="btn btn-info btn-sm">VIP</a>
                        <a href="<?php echo ROOT_URL.'home/supervip/'.$item['id']; ?>" onclick="return confirm('Цена Super Vip-а составляет 50 Рублей');" class="btn  btn-primary btn-sm">SUPER VIP</a>
                        <div class="alert-warning btn-sm"><?= $item['date']; ?></div>
                        <a class="alert-success btn-sm"><?= $item['userId']; ?> | <?= $item['user_name']; ?></a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-5">
                    <div class="all-products-images-div">
                        <img src="<?php echo $image; ?>" class="<?php echo $prodImgClass; ?>" />
                    </div>
                </div>
                <div class="col-lg-9 col-md-7">
                    <a href="<?php echo ROOT_URL . 'car/view/' . $item['id']; ?>" class="product-url">
                        <div class="row">
                            <div class="col-12">
                                <h2 class="h2-product-name" itemprop="name"><?= $item['car_name']; ?> <?= $item['car_model']; ?></h2>
                                <div class="row" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                                    <div class="col-6 col-md-4 col-lg-3">
                                        <img src="<?= SELECT_SHARED_IMAGE . 'icons/calendar.svg'; ?>"
                                             class="products-icons-15"/>
                                        <span><?= $item['year']; ?> <?= $lang['YEAR']; ?></span>
                                    </div>
                                    <div class="col-6 col-md-4 col-lg-3">
                                        <img src="<?= SELECT_SHARED_IMAGE . 'icons/engine.svg'; ?>"
                                             class="products-icons-15"/>
                                        <span><?= $item['cars_motor_size_id']; ?> <?= $lang[$item['petrol_type']]; ?></span>
                                    </div>
                                    <div class="col-6 col-md-4 col-lg-3">
                                            <span>
                                                <img src="<?= SELECT_SHARED_IMAGE . 'icons/run.svg'; ?>"
                                                     class="products-icons-15"/>
                                                <?= $item['run']; ?> <?= $lang[$item['run_type']]; ?></span>
                                    </div>
                                    <div class="col-6 col-md-4 col-lg-3">
                                        <img src="<?= SELECT_SHARED_IMAGE . 'icons/rudder.svg'; ?>"
                                             class="products-icons-15"/>
                                        <span><?= $lang[$item['rudder']]; ?></span>
                                    </div>
                                    <div class="col-6 col-md-4 col-lg-3">
                                            <span>
                                                <img src="<?= SELECT_SHARED_IMAGE . 'icons/gearbox.svg'; ?>"
                                                     class="products-icons-15"/>
                                                <span><?= $lang[$item['transmission']]; ?></span>
                                            </span>
                                    </div>
                                    <div class="col-6 col-md-4 col-lg-3">
                                            <span itemprop="priceCurrency" content="RUB">
                                                <img src="<?= SELECT_SHARED_IMAGE . 'icons/ruble.svg'; ?>"
                                                     class="products-icons-15"/>
                                                <span itemprop="price"
                                                      content="<?= $item['price']; ?>"><?= $item['price']; ?></span>
                                            </span>
                                    </div>
                                    <div class="col-sm-12 m-t-15 description" itemprop="description">
                                        <?php
                                        $length = 290;
                                        $string = strip_tags($item['description']);
                                        if (strlen($string) > $length) {
                                            $stringCut = substr($string, 0, $length);
                                            $string = substr($stringCut, 0, strrpos($stringCut, ' ')) . ' ...';
                                        }
                                        echo $string;
                                        ?>
                                    </div>
                                </div>
                            </div>

                            <h5 class="product-date">
                                <label class="label label-warning product-date-label">06-02-2020</label><br>
                            </h5>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <?php
    }
}
?>