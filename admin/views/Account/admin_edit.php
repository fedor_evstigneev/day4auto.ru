<form action="" method="post" class="edit-users-form">
	<div class="form-group">
		<label for="name">Имя Фамилия:</label>
		<input type="text" name="name" value="<?php echo $viewmodel['name']; ?>" class="form-control" id="name"/>
	</div>
	
	<div class="form-group">
		<label for="mail">Эл. Почта:</label>
		<input type="text" name="mail" value="<?php echo $viewmodel['mail']; ?>" class="form-control" id="mail"/>
	</div>
	
	<div class="form-group">
		<label for="phone">Телефон:</label>
		<input type="text" name="phone" value="<?php echo $viewmodel['phone']; ?>" class="form-control" id="phone"/>
	</div>
	
	<div class="form-group">
		<label for="password">Пароль:</label>
		<input type="text" name="password" value="<?php echo $viewmodel['password']; ?>" class="form-control" id="password" disabled/>
	</div>
	<div class="form-group">
		<input type="submit" name="save" value="Сохранить" class="btn btn-success"/>
	</div>

</form>