<h3>Администраторы !</h3><br/>
<?php
if(count($viewmodel) >= 1){
    foreach($viewmodel as $item){
        ?>
        <div class="row border-1-ccc m-t-10">
            <div class="col-8">
                <div class="row">
                    <div class="col-4"><b>ID</b></div>
                    <div class="col-8"><?php echo $item['id']; ?></div>
                    <div class="col-12"><hr/></div>
                    <div class="col-4"><b>Телефон:</b></div>
                    <div class="col-8"><?php echo $item['phone']; ?></div>
                    <div class="col-12"><hr/></div>
                    <div class="col-4"><b>Эл. почта:</b></div>
                    <div class="col-8"><?php echo $item['mail']; ?></div>
                </div>
            </div>
            <div class="col-4 center my-auto">
                <a href="<?php echo ROOT_URL.'account/admin_edit/'.$item['id']; ?>" class="btn btn-warning btn-sm">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                </a>
                <a href="<?php echo ROOT_URL.'account/admin_delete/'.$item['id']; ?>" onclick="return confirm('Вы уверины что хотите удалить этого пользователя ?');" class="btn btn-danger btn-sm">
                    <i class="fa fa-trash" aria-hidden="true"></i>
                </a>
            </div>
        </div>
        <?php
    }
}
?>