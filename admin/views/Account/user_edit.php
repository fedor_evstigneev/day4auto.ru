<?php
$AdsModel = new AdsModel();
$city = $AdsModel->city();
?>

<form action="" method="post" class="edit-users-form">
	<div class="form-group">
		<label for="nameSurname">Имя Фамилия:</label>
		<input type="text" name="nameSurname" value="<?php echo $viewmodel['nameSurname']; ?>" class="form-control" id="nameSurname"/>
	</div>
	
	<div class="form-group">
		<label for="mail">Эл. Почта:</label>
		<input type="text" name="mail" value="<?php echo $viewmodel['mail']; ?>" class="form-control" id="mail"/>
	</div>
	
	<div class="form-group">
		<label for="RegCountry">Выберите свой город:</label>
		<select id="RegCountry" name="city" required>
<?php
	
			foreach($city as $cities){
?>

				<option value="<?php echo $cities['city']; ?>" <?php if($viewmodel['city'] == $cities['city']){echo 'selected';} ?>><?php echo $cities['city']; ?></option>
<?php
			}
?>				
		</select>
	</div>
	
	<div class="form-group">
		<label for="phone">Телефон:</label>
		<input type="text" name="phone" value="<?php echo $viewmodel['phone']; ?>" class="form-control" id="phone"/>
	</div>
	
	<div class="form-group">
		<label for="password">Пароль:</label>
		<input type="text" name="password" value="<?php echo $viewmodel['password']; ?>" class="form-control" id="password" disabled/>
	</div>
	<div class="form-group">
		<input type="submit" name="save" value="Сохранить" class="btn btn-success"/>
	</div>

</form>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.bootstrap2.css" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/js/standalone/selectize.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#mainCountry').selectize();
			$('#mainCountry').change(function(){
				$("#countryForm").submit();
			});
			
			$('#RegCountry').selectize();
			$('#RegCountry').change(function(){
				$("#countryForm").submit();
			});
			
			$('#catFilter').change(function(){
				$("#filters").submit();
			});
			$('#catFilter').selectize({
				 plugins: ['remove_button']
			});
		});
	</script>