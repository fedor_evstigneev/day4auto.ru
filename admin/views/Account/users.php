<form action="" method="post" class="filter-full-form">
    <div class="form-group">
        <label for="text">ID, Эл. почта, город или телефон</label>
        <input type="text" name="text" class="form-control" id="text" value="<?php echo (isset($_POST['text'])? $_POST['text']: ''); ?>" />
    </div>
    <div class="form-group">
        <input type="submit" name="search" value="Поиск !" class="btn btn-success" />
    </div>
</form>

<?php
	$AcountsModel = new AccountModel();
	
	$idsForPagination = $AcountsModel->selectUsersID();
	
	
	if(!empty($idsForPagination) >=1){
		$numbers = $AcountsModel->Pagination($idsForPagination);
	}
?>

 Всего пользватетелй <span class="alert btn-sm alert-warning"><?php echo $userCounter->Counter('users', ''); ?></span> |  Активные <span class="alert btn-sm alert-success"><?php echo $userCounter->Counter('users', 'WHERE block = 0'); ?></span> |  Заблокированные <span class="alert btn-sm alert-danger"><?php echo $userCounter->Counter('users', 'WHERE block = 1'); ?></span>

<h3>Пользователи !</h3><br/>


<?php
    if(count($viewmodel) >= 1){
        foreach($viewmodel as $item){
?>
<div class="row border-1-ccc m-t-10">
    <div class="col-8 b-r-1-ccc">
        <div class="row">
            <div class="col-4"><b>ID</b></div>
            <div class="col-8"><?php echo $item['id']; ?></div>
            <div class="col-12"><hr/></div>
            <div class="col-4"><b>ФИО:</b></div>
            <div class="col-8"><?php echo $item['nameSurname']; ?></div>
            <div class="col-12"><hr/></div>

            <div class="col-4"><b>Город:</b></div>
            <div class="col-8"><?php echo $item['city']; ?></div>
            <div class="col-12"><hr/></div>
            <div class="col-4"><b>Телефон:</b></div>
            <div class="col-8"><?php echo $item['phone']; ?></div>
            <div class="col-12"><hr/></div>
            <div class="col-4"><b>Сайт:</b></div>
            <div class="col-8"><?php echo $item['fromSite']; ?></div>
            <div class="col-12"><hr/></div>
            <div class="col-4"><b>Эл. почта:</b></div>
            <div class="col-8"><?php echo $item['mail']; ?></div>
        </div>
    </div>
    <div class="col-4 center my-auto">
        <a href="<?php echo ROOT_URL.'account/User_edit/'.$item['id']; ?>" class="btn btn-info btn-sm">
            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
        </a>
        <?php
        if($item['block'] == 0){
            ?>
            <a href="<?php echo ROOT_URL.'account/user_block/'.$item['id'].'-1/'; ?>" onclick="return confirm('Вы уверины что хотите заблокировать этого пользователя ?');" class="btn btn-warning btn-sm">
                <i class="fa fa-ban" aria-hidden="true" title="Заблок"></i>
            </a>
            <?php
        }elseif($item['block'] == 1){
            ?>
            <a href="<?php echo ROOT_URL.'account/user_block/'.$item['id'].'-0/'; ?>" onclick="return confirm('Вы уверины что хотите разблокировать этого пользователя ?');" class="btn btn-success btn-sm">
                <i class="fa fa-ban" aria-hidden="true"></i> Разблок
            </a>
            <?php
        }
        ?>
        <a href="<?php echo ROOT_URL.'account/user_delete/'.$item['id']; ?>" onclick="return confirm('Вы уверины что хотите удалить этого пользователя ?');" class="btn btn-danger btn-sm">
            <i class="fa fa-trash" aria-hidden="true"></i>
        </a>
    </div>
</div>
<?php
        }
    }
?>





<div class="clear"></div>
<ul class="pagination">
<?php
if(empty($_GET['id'])){$_GET['id'] = 1;}
$maxGet = $_GET['id'] + 3;
if($_GET['id'] <= 3){$minGet = 1;}else{$minGet = $_GET['id'] - 3;}

if(!empty($numbers)){
	if($_GET['id'] != 1){
?>
		<li><a href="<?php echo ROOT_URL.'ads/technics_other/';  echo $_GET['id'] == 1?1:$_GET['id'] - 1; ?>/"><<</a></li>
<?php
	}
?>
	<li <?php if($_GET['id'] == 1){echo 'class="active"';} ?>><a href="<?php echo ROOT_URL.'acounts/users/1/'; ?>">1</a></li>
<?php
	if(($minGet + 1) < count($numbers) AND $_GET['id'] >= 4){
		echo '<li><span>...</span></li>';
	}
	foreach($numbers as $num){
		if($num !=1 AND $num != count($numbers)){
			if($minGet <= $num AND $num <= $maxGet ){
?>
			<li <?php if($_GET['id'] == $num){echo 'class="active"';} ?>><a href="<?php echo ROOT_URL.'acounts/users/'.$num.'/'; ?>"><?php echo $num; ?></a></li>
<?php
			}
		}
	}
	if(($maxGet -1) < count($numbers)){
		echo '<li><span>...</span></li>';
	}
	if(count($numbers) >=2){
?>
	<li <?php if($_GET['id'] == count($numbers)){echo 'class="active"';} ?>><a href="<?php echo ROOT_URL.'acounts/users/'.count($numbers).'/'; ?>"><?php echo count($numbers); ?></a></li>
<?php
	}
	if($_GET['id'] != count($numbers)){
?>
		<li><a href="<?php echo ROOT_URL.'acounts/users/';  echo $_GET['id'] == count($numbers)?$_GET['id']:$_GET['id'] + 1; ?>/">>></a></li>
<?php
	}
}
?>	
</ul>