<?php
session_start();
    // session_unset();
    // session_destroy();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//include config
require('config.php');

foreach (glob("classes/*.php") as $filename){
    include $filename;
}

foreach (glob("controllers/*.php") as $filename){
    include $filename;
}

foreach (glob("models/*.php") as $filename){
    include $filename;
}

$bootstrap = new Bootstrap($_GET);
$controller = $bootstrap->createController();
if($controller){
	$controller->executeAction();
}