<?php
class HomeModel extends Model{
	public function Index(){
        $per_page = 15;

        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        if(isset($post['mail']) && isset($post['password'])){
            return $this->login();
        }

        return $this->selectAuto($per_page);
	}

	public function login(){
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        $password = MD5($post['password']);
        $this->query('SELECT * FROM u0555183_merged.admin WHERE mail = :mail AND password = :password');
        $this->bind(':mail', $post['mail']);
        $this->bind(':password', $password);
        $row = $this->single();
        if($row){
            $_SESSION['admin_logged_in'] = true;
            $_SESSION['admin_data'] = array(
                "id"		=>$row['id'],
                "nameSurname"		=>$row['name'],
                "mail"		=>$row['mail'],
                "phone"		=>$row['phone']
            );
            header('location:'.ROOT_URL);
        }else{
            Messages::setMsg('Incorrect Login', 'error');
        }
    }

    public function selectAuto($per_page = 10){
        $get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);

        if(!empty($get['id'])){$current_page = $get['id'];}else{$current_page = 1;}

        $param1 = ($current_page - 1) * $per_page;
        $this->query('SELECT cars_name.name as car_name, cars.on_off, cars.userId, cars_models.name as car_model, cars.id, cars.images, cars.views, cars.price, cars.year, cars.description, cars.date, 
                            cars.accessories, cars.run, cars.user_name, cars.phone, 
                            cars_motor_size.name as cars_motor_size_id, cars_petrol_type.name as petrol_type, rudder.name as rudder, transmission.name as transmission, `u0555183_merged`.`city`.`city` as city,
                            run_type.name as run_type
                            FROM cars
                            LEFT JOIN cars_name ON cars.cars_name_id = cars_name.id
                            LEFT JOIN run_type ON cars.run_type = run_type.id
                            LEFT JOIN `u0555183_merged`.`city` ON cars.city = city.id
                            LEFT JOIN cars_models ON cars.cars_models_id = cars_models.id
                            LEFT JOIN cars_motor_size ON cars.cars_motor_size_id = cars_motor_size.id
                            LEFT JOIN cars_petrol_type ON cars.petrol_type = cars_petrol_type.id
                            LEFT JOIN rudder ON cars.rudder = rudder.id
                            LEFT JOIN transmission ON cars.transmission = transmission.id
                            ORDER BY id DESC LIMIT :param1, :per_page');
        $this->bind('param1', $param1);
        $this->bind('per_page', $per_page);
        $rows = $this->resultSet();
        return $rows;
    }

    public function Delete(){
        $get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
        $this->query('DELETE FROM cars WHERE id = :id');
        $this->bind('id', $get['id']);
        $this->execute();
        if($this->rowCount() == 1){
            header('location: '.$_SERVER['HTTP_REFERER']);
        }else{
            Messages::setMsg('Это убъявление уже было удаленно или произошла ошибка', 'error');
            header('location: '.ROOT_URL);
        }
    }


    ######### Включение и выключение объявлений Начало ###########

    public function On(){
        $this->on_off('on', 'Объявление успешно включено');
    }

    public function Off(){
        $this->on_off('off', 'Объявление успешно отключено');
    }

    public function on_off($action, $message){
        $get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
        $this->query('UPDATE cars SET on_off = "'.$action.'" WHERE id = :id');
        $this->bind('id', $get['id']);
        $this->execute();
        if($this->rowCount()){
            Messages::setMsg($message, 'success');
            header('location: '.$_SERVER['HTTP_REFERER']);
        }else{
            Messages::setMsg('Произошла не известная ошибка', 'error');
            header('location: '.$_SERVER['HTTP_REFERER']);
        }
    }

    ######### Включение и выключение объявлений Конец ###########


    ######### Вип и супер вип Начало ###########

    public function Vip(){
        $this->vip_superVip(1, 'Объявление получила статус VIP');
    }

    public function SuperVip(){
        $this->vip_superVip(2, 'Объявление получила статус SUPER VIP');
    }

    public function vip_superVip($action, $message){
        $get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
        $this->query('UPDATE cars SET vipStatus = "'.$action.'" WHERE id = :id');
        $this->bind('id', $get['id']);
        $this->execute();
        if($this->rowCount()){
            Messages::setMsg($message, 'success');
            header('location: '.$_SERVER['HTTP_REFERER']);
        }else{
            Messages::setMsg('Произошла не известная ошибка', 'error');
            header('location: '.$_SERVER['HTTP_REFERER']);
        }
    }

    ######### Вип и супер вип Конец ###########

}