<?php
class LangModel extends Model{
	public function Index(){
		$this->query('SELECT * FROM `u0555183_merged`.languages');
		$rows = $this->resultSet();
		return $rows;
	}
	
	public function Del(){
		$get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
		
		if(isset($get['id'])){
			
			$this->query('SELECT * FROM `u0555183_merged`.languages WHERE id = :id');
			$this->bind('id', $get['id']);
			$row = $this->single();
			
			$imageDel = UPLOAD_SHARED_IMAGE.'flag/'.$row['img'];
			unlink($imageDel);
			
			$this->query('DELETE FROM `u0555183_merged`.languages WHERE id = :id');
			$this->bind('id', $get['id']);
			$this->execute();
			if($this->rowCount() == 1){
				//Redirect
				header('location: '.$_SERVER['HTTP_REFERER']);
			}else{
				header('location: '.$_SERVER['HTTP_REFERER']);
			}
		}
	}
	
	public function Add(){
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
		
		if(isset($post['save'])){
			
			$img = $this->ImgProcessor();
			
			$this->query('INSERT INTO `u0555183_merged`.languages (display_name, determinant, culture_name, img) VALUES (:display_name, :determinant, :culture_name, :img)');
			$this->bind('display_name', $post['display_name']);
			$this->bind('culture_name', $post['culture_name']);
			$this->bind('determinant', $post['determinant']);
			$this->bind('img', $img);
			$this->execute();
			if($this->lastInsertId()){
				header('location:'.ROOT_URL.'lang/');
			}
		}
	}
	
	// სურათების პროცესორი
	public $imageNamesSum;
	
	public function ImgProcessor(){	
		foreach($_FILES['image']['tmp_name'] as $key => $tmp_name ){
			//ვქმნით ფაილის მონაცემებს
			$file = $_FILES['image']['name'][$key];
			$tmp = $_FILES['image']['tmp_name'][$key];
			$filesize = $_FILES['image']['size'][$key];
			$size = floor($filesize / 1024);
			$ext = pathinfo($file, PATHINFO_EXTENSION);
			$ext = strtolower($ext);
				
				if($ext == "jpg" || $ext == "jpeg"){
					$src = imagecreatefromjpeg($tmp);
				}
				elseif($ext == "png"){
					$src = imagecreatefrompng($tmp);
				}
				else{
					$src = imagecreatefromgif($tmp);
				}
				
				list($width, $height) = getimagesize($tmp);
				
				if($width > $height){
					$new_width = 25;
					$new_height = ($height / $width) * $new_width;
				}
				else{
					$new_width = 25;
					$new_height = ($height / $width) * $new_width;
				}				
				
				$resize = imagecreatetruecolor($new_width, $new_height);
				imagecopyresampled($resize, $src, 0,0,0,0, $new_width, $new_height, $width, $height);
				
				$dir = UPLOAD_SHARED_IMAGE.'/flag/';
				$filename = time().MD5($_FILES["image"]["name"][$key]).'.'.$ext;
				$full = $dir.$filename;
				
				imagejpeg($resize,$full,80);
				/*
				echo '<strong>Orig File:</strong> '.$file.'<br />';
				echo '<strong>Tmp Name:</strong> '.$tmp.'<br />';
				echo '<strong>Extension:</strong> '.$ext.'<br />';
				echo '<strong>Width x Height:</strong> '.$width.' X '.$height.'<br />';
				echo '<strong>New Width x Height:</strong> '.$new_width.' X '.$new_height.'<br />';
				echo '<strong>Size:</strong> '.$size.'<br />';
				echo '<strong>New File Name:</strong> '.$full.'<br />';
				echo '<br /><img src="'.$full.'" /><br /><br />';
				*/
				$imageNames[] = $filename;
				$imageNamesSum = implode(',', $imageNames);

			}
		return $imageNamesSum;
	}
}