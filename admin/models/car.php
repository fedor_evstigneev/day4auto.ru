<?php

class CarModel extends Model
{
    public function Size()
    {
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        if(isset($post['add'])){
            $this->add_details('cars_motor_size');
            header('location:'.ROOT_URL.'car/size/');
        }
        return $this->selecAll('cars_motor_size');
    }

    public function Door()
    {
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        if(isset($post['add'])){
            $this->add_details('door');
            header('location:'.ROOT_URL.'car/door/');
        }
        return $this->selecAll('door');
    }

    public function Cylinder()
    {
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        if(isset($post['add'])){
            $this->add_details('celinder');
            header('location:'.ROOT_URL.'car/cylinder/');
        }
        return $this->selecAll('celinder');
    }

    public function Gearbox()
    {
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        if(isset($post['add'])){
            $id = $this->add_translations();
            $this->add_translations_details('transmission', $id);
            header('location:'.ROOT_URL.'car/gearbox/');
        }
        return $this->selecAll('transmission');
    }

    public function Petrol()
    {
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        if(isset($post['add'])){
            $id = $this->add_translations();
            $this->add_translations_details('cars_petrol_type', $id);
            header('location:'.ROOT_URL.'car/petrol/');
        }
        return $this->selecAll('cars_petrol_type');
    }

    public function Run_type()
    {
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        if(isset($post['add'])){
            $id = $this->add_translations();
            $this->add_translations_details('run_type', $id);
            header('location:'.ROOT_URL.'car/run_type/');
        }
        return $this->selecAll('run_type');
    }

    public function Rudder()
    {
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        if(isset($post['add'])){
            $id = $this->add_translations();
            $this->add_translations_details('rudder', $id);
            header('location:'.ROOT_URL.'car/rudder/');
        }
        return $this->selecAll('rudder');
    }

    public function Category()
    {
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        if(isset($post['add'])){
            $id = $this->add_translations();
            $this->add_translations_details('category', $id);
            header('location:'.ROOT_URL.'car/category/');
        }
        return $this->selecAll('category');
    }

    public function Airbag()
    {
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        if(isset($post['add'])){
            $this->add_details('airbag');
            header('location:'.ROOT_URL.'car/airbag/');
        }
        return $this->selecAll('airbag');
    }

    public function Accessories()
    {
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        if(isset($post['add'])){
            $id = $this->add_translations();
            $this->add_translations_details('accessories', $id);
            header('location:'.ROOT_URL.'car/accessories/');
        }
        return $this->selecAll('accessories');
    }

    public function selecAll($table = '')
    {
        $this->query('SELECT * FROM ' . $table . '  ORDER BY id DESC');
        $rows = $this->resultSet();
        return $rows;
    }

    public function add_translations()
    {
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        $this->query('INSERT INTO `u0555183_merged`.translations (shorten, Trans, langId) VALUES (:name, :name, 1)');
        $this->bind('name', $post['name']);
        $this->execute();
        $transLastInsertId = $this->lastInsertId();
        if ($transLastInsertId) {
            $this->query('UPDATE `u0555183_merged`.translations SET shorten = :name WHERE id = "'.$transLastInsertId.'"');
            $this->bind('name', str_replace(' ', '_', $post['name']).'-'.$transLastInsertId);
            $this->execute();
            return $transLastInsertId;
        }
    }

    public function add_translations_details($table = '', $id = '')
    {
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        $this->query('INSERT INTO '.$table.' (name) VALUES (:name)');
        $this->bind('name', str_replace(' ', '_', $post['name']).'-'.$id);
        $this->execute();
        return $this->lastInsertId();
    }

    public function add_details($table = '')
    {
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        $this->query('INSERT INTO '.$table.' (name) VALUES (:name)');
        $this->bind('name', str_replace(' ', '_', $post['name']));
        $this->execute();
        return $this->lastInsertId();
    }

//    Delete functions

    public function Size_delete()
    {
        $get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
        $this->delete_details('cars_motor_size WHERE id = "' . $get['id'] . '"');
        header('location:' . ROOT_URL . 'car/size/');
    }

    public function Cylinder_delete()
    {
        $get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
        $this->delete_details('celinder WHERE id = "' . $get['id'] . '"');
        header('location:' . ROOT_URL . 'car/cylinder/');
    }

    public function Door_delete()
    {
        $get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
        $this->delete_details('door WHERE id = "' . $get['id'] . '"');
        header('location:' . ROOT_URL . 'car/door/');
    }

    public function Petrol_delete()
    {
        $get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
        $acc = $this->selecAll('cars_petrol_type WHERE id ="' . $get['id'] . '"');
        $this->delete_details('`u0555183_merged`.translations WHERE shorten = "' . $acc[0]['name'] . '"');
        $this->delete_details('cars_petrol_type WHERE id = "' . $get['id'] . '"');
        header('location:' . ROOT_URL . 'car/petrol/');
    }

    public function Run_type_delete()
    {
        $get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
        $acc = $this->selecAll('run_type WHERE id ="' . $get['id'] . '"');
        $this->delete_details('`u0555183_merged`.translations WHERE shorten = "' . $acc[0]['name'] . '"');
        $this->delete_details('run_type WHERE id = "' . $get['id'] . '"');
        header('location:' . ROOT_URL . 'car/run_type/');
    }

    public function Rudder_delete()
    {
        $get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
        $acc = $this->selecAll('rudder WHERE id ="' . $get['id'] . '"');
        $this->delete_details('`u0555183_merged`.translations WHERE shorten = "' . $acc[0]['name'] . '"');
        $this->delete_details('rudder WHERE id = "' . $get['id'] . '"');
        header('location:' . ROOT_URL . 'car/rudder/');
    }

    public function Airbag_delete()
    {
        $get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
        $this->delete_details('airbag WHERE id = "' . $get['id'] . '"');
        header('location:' . ROOT_URL . 'car/airbag/');
    }

    public function Accessories_delete()
    {
        $get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
        $acc = $this->selecAll('accessories WHERE id ="' . $get['id'] . '"');
        $this->delete_details('`u0555183_merged`.translations WHERE shorten = "' . $acc[0]['name'] . '"');
        $this->delete_details('accessories WHERE id = "' . $get['id'] . '"');
        header('location:' . ROOT_URL . 'car/accessories/');
    }

    public function Gearbox_delete()
    {
        $get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
        $acc = $this->selecAll('transmission WHERE id ="' . $get['id'] . '"');
        $this->delete_details('`u0555183_merged`.translations WHERE shorten = "' . $acc[0]['name'] . '"');
        $this->delete_details('transmission WHERE id = "' . $get['id'] . '"');
        header('location:' . ROOT_URL . 'car/gearbox/');
    }

    public function Category_delete()
    {
        $get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
        $acc = $this->selecAll('category WHERE id ="' . $get['id'] . '"');
        $this->delete_details('`u0555183_merged`.translations WHERE shorten = "' . $acc[0]['name'] . '"');
        $this->delete_details('category WHERE id = "' . $get['id'] . '"');
        header('location:' . ROOT_URL . 'car/category/');
    }

    public function delete_details($table)
    {
        $this->query('DELETE FROM ' . $table);
        $this->execute();
    }

    //    Edit functions

    public function Size_edit()
    {
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        $item = $this->select_single('cars_motor_size');
        if (isset($post['save'])) {
            $this->default_edit('cars_motor_size');
            header('location:'.ROOT_URL.'car/size/');
        }
        return $item;
    }

    public function Door_edit()
    {
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        $item = $this->select_single('door');
        if (isset($post['save'])) {
            $this->default_edit('door');
            header('location:'.ROOT_URL.'car/door/');
        }
        return $item;
    }

    public function Cylinder_edit()
    {
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        $item = $this->select_single('celinder');
        if (isset($post['save'])) {
            $this->default_edit('celinder');
            header('location:'.ROOT_URL.'car/cylinder/');
        }
        return $item;
    }

    public function Gearbox_edit()
    {
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        $item = $this->select_single('transmission');
        if (isset($post['save'])) {
            $this->translations_edit($item['name']);
            header('location:'.ROOT_URL.'car/gearbox/');
        }
        return $item;
    }

    public function Petrol_edit()
    {
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        $item = $this->select_single('cars_petrol_type');
        if (isset($post['save'])) {
            $this->translations_edit($item['name']);
            header('location:'.ROOT_URL.'car/petrol/');
        }

        return $item;
    }

    public function Run_type_edit()
    {
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        $item = $this->select_single('run_type');
        if (isset($post['save'])) {
            $this->translations_edit($item['name']);
            header('location:'.ROOT_URL.'car/run_type/');
        }

        return $item;
    }

    public function Accessories_edit()
    {
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        $item = $this->select_single('accessories');
        if (isset($post['save'])) {
            $this->translations_edit($item['name']);
            header('location:'.ROOT_URL.'car/accessories/');
        }
        return $item;
    }

    public function Rudder_edit()
    {
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        $item = $this->select_single('rudder');
        if (isset($post['save'])) {
            $this->translations_edit($item['name']);
            header('location:'.ROOT_URL.'car/rudder/');
        }
        return $item;
    }

    public function Category_edit()
    {
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        $item = $this->select_single('category');
        if (isset($post['save'])) {
            $this->translations_edit($item['name']);
            header('location:'.ROOT_URL.'car/category/');
        }
        return $item;
    }

    public function Airbag_edit()
    {
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        $item = $this->select_single('airbag');
        if (isset($post['save'])) {
            $this->default_edit('airbag');
            header('location:'.ROOT_URL.'car/airbag/');
        }
        return $item;
    }

    public function select_single($table = '')
    {
        $get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);

        $this->query('SELECT * FROM ' . $table . ' WHERE id = :id');
        $this->bind('id', $get['id']);
        $row = $this->single();
        return $row;
    }

    public function translations_edit($shorten = '')
    {
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        $this->query('UPDATE `u0555183_merged`.translations SET Trans = :trans WHERE shorten = :shorten && langID = 1');
        $this->bind('shorten', $shorten);
        $this->bind('trans', $post['name']);
        $this->execute();
        return;
    }

    public function default_edit($table = ''){
        $get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        $this->query('UPDATE '.$table.' SET name = :name WHERE id = :id');
        $this->bind('id', $get['id']);
        $this->bind('name', $post['name']);
        $this->execute();
        return;
    }
}
?>