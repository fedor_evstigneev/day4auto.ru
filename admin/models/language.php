<?php
class LanguageModel extends Model{
	public function Select(){
		$get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
		
		if(isset($get['id'])){
			$_SESSION['lang'] = $get['id'];
		}elseif(isset($_SESSION['lang'])){
			
		}else{
			$_SESSION['lang'] = 'ru';
		}

		$this->query('SELECT * FROM u0555183_merged.languages WHERE determinant = :determinant');
		$this->bind(':determinant', $_SESSION['lang']);
		$langRows = $this->resultSet();
		
		foreach($langRows as $languages){
			$culture = $languages['culture_name'];
		}
		if(isset($_GET['id'])){
			if(isset($_SERVER['HTTP_REFERER'])){
				header('Location: ' . $_SERVER['HTTP_REFERER']);
			}else{
				header('Location: '.ROOT_URL);
			}
			
		}
		$_SESSION['culture'] = $culture;
	}
	
	public function EngComparsion(){
		$this->query('SELECT * FROM u0555183_merged.translations WHERE langID = "1"');
		$rows = $this->resultSet();
		
		foreach($rows as $trans){
			$lang[$trans['shorten']] = $trans['Trans'];
		}
		return $lang;
	}
	
	public function SelectLanguage(){
		$this->query('
			SELECT a.*, b.* 
			FROM u0555183_merged.translations a
			INNER JOIN u0555183_merged.languages b
			ON a.langID = b.id
			WHERE b.culture_name = :culture
		');
		
		$this->bind(':culture', $_SESSION['culture']);
		$rows = $this->resultSet();
		
		if(empty($rows)){
			$langs = $this->EngComparsion();
		}else{
			foreach($rows as $trans){
				$lang[$trans['shorten']] = $trans['Trans'];
			}
			$langs = array_merge($this->EngComparsion(), $lang);
		}
		return $langs;
	}

	
	public function allLangs(){
		$this->query('SELECT * FROM u0555183_merged.languages');
		$langs = $this->resultSet();
		return $langs;
	}
}