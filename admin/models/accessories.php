<?php
class AccessoriesModel extends Model{
	public function Index(){
		$this->query('SELECT * FROM accessories ORDER BY id DESC');
		$rows = $this->resultSet();
		return $rows;
	}

	public function Add(){
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        if(isset($post['save']) && isset($post['name'])){
            $this->query('INSERT INTO `u0555183_merged`.translations (shorten, Trans, langId) VALUES (:name, :name, 1)');
            $this->bind('name', $post['name']);
            $this->execute();
            $transLastInsertId = $this->lastInsertId();
            if($transLastInsertId){

                $this->query('UPDATE `u0555183_merged`.translations SET shorten = :name WHERE id = "'.$transLastInsertId.'"');
                $this->bind('name', $post['name'].'-'.$transLastInsertId);
                $this->execute();

                $this->query('INSERT INTO accessories (name) VALUES (:name)');
                $this->bind('name', $post['name'].'-'.$transLastInsertId);
                $this->execute();
                if($this->lastInsertId()){
                    Messages::setMsg('Аксесуар успешно добавлен!', 'success');
                }else{
                    Messages::setMsg('Во время добавления аксесуара произошла ошибка!', 'error');
                }
                header('location:'.ROOT_URL.'accessories');
            }
        }
    }

    public function Delete(){
        $get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);

        $this->query('SELECT name FROM accessories WHERE id = :id');
        $this->bind('id', $get['id']);
        $name = $this->single();

        if(!empty($name)){
            $this->query('DELETE FROM `u0555183_merged`.translations WHERE shorten = "'.$name['name'].'"');
            $this->execute();

            $this->query('DELETE FROM accessories WHERE id = :id');
            $this->bind('id', $get['id']);
            $this->execute();
            if($this->rowCount()){
                Messages::setMsg('Аксесуар успешно удален!', 'success');
            }else{
                Messages::setMsg('Во время удаления произошла ошибка!', 'error');
            }
            header('location:'.ROOT_URL.'accessories');
        }
    }

    public function Edit(){
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        if(isset($post['save'])){
            $thisAccesorie = $this->ThisAccessorie();
            $this->query('UPDATE `u0555183_merged`.translations SET Trans = :trans WHERE shorten = "'.$thisAccesorie['name'].'"');
            $this->bind('trans', $post['name']);
            $this->execute();
            if($this->rowCount()){
                Messages::setMsg('Аксесуар успешно изменен!', 'success');
            }else{
                Messages::setMsg('Во время изменения произошла ошибка!', 'error');
            }
            header('location:'.ROOT_URL.'accessories');
        }
        return $this->ThisAccessorie();
    }

    public function ThisAccessorie(){
        $get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
        $this->query('SELECT * FROM accessories WHERE id = :id');
        $this->bind('id', $get['id']);
        return $name = $this->single();
    }

    public function Translate(){
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        $get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
        $accessories = $this->ThisAccessorie();
        if(isset($post['save'])){
            $this->query('INSERT INTO `u0555183_merged`.translations (shorten, Trans, langID) VALUES (:shorten, :Trans, :langID)');
            $this->bind('shorten', $accessories['name']);
            $this->bind('Trans', $post['Trans']);
            $this->bind('langID', $post['langID']);
            $this->execute();
            if($this->lastInsertId()){
                header('location:'.ROOT_URL.'accessories');
            }else{
                Messages::setMsg('Перевод не слова не выполнен :( ', 'error');
            }
        }

        $this->query('SELECT * FROM `u0555183_merged`.translations WHERE shorten = :shorten');
        $this->bind('shorten', $accessories['name']);
        $rows = $this->resultSet();
        return $rows;
    }

    public function translateLang(){
        $this->query('SELECT * FROM `u0555183_merged`.languages WHERE id != 1');
        $rows = $this->resultSet();
        return $rows;
    }
}
?>