<?php
class AccountModel extends Model{
	public function Admins(){
		$this->query('SELECT * FROM u0555183_merged.admin');
		$rows = $this->resultSet();
		return $rows;
	}
	
	public function Users(){
		$per_page = 100;
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        $get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
		
        if(!empty($get['id'])){
            $current_page = $get['id'];
        }else{
            header('location:'.ROOT_URL.'account/users/1/');
        }
		if(!empty($get['id']) AND is_numeric($get['id'])){$current_page = $get['id'];}else{$current_page = 1;}
		
		$param1 = ($current_page - 1) * $per_page;
		
		
		if(isset($post['search'])){
			$this->query('SELECT * FROM u0555183_merged.users WHERE id = :text OR mail = :text OR phone = :text OR nameSurname LIKE "%'.$post['text'].'%"');
			$this->bind('text', $post['text']);
			$rows = $this->resultSet();
			return $rows;
		}
		
		$this->query('SELECT * FROM u0555183_merged.users ORDER BY id DESC  LIMIT :param1, :per_page');
		$this->bind('param1', $param1);
        $this->bind('per_page', $per_page);
		$rows = $this->resultSet();
		return $rows;
	}
	
	public function Admin_edit(){
		$get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
		
		if(isset($post['save'])){
			$this->query('UPDATE u0555183_merged.admin SET name = :name, phone = :phone, mail = :mail WHERE id = :id');
			$this->bind('name', $post['name']);
			$this->bind('phone', $post['phone']);
			$this->bind('mail', $post['mail']);
			$this->bind('id', $get['id']);
			$this->execute();
			header('location:'.ROOT_URL.'account/admins/');
		}
		
		if(isset($get['id'])){
			$this->query('SELECT * FROM u0555183_merged.admin WHERE id = :id');
			$this->bind('id', $get['id']);
			$row = $this->single();
			return $row;
		}
	}
	
	public function Admin_delete(){
		$get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
		
		if(isset($get['id'])){
			$this->query('DELETE FROM u0555183_merged.admin WHERE id = :id');
			$this->bind('id', $get['id']);
			$this->execute();
			if($this->rowCount() == 1){
					//Redirect
					header('location: '.ROOT_URL.'account/admins/');
				}else{
					header('location: '.ROOT_URL.'account/admins/');
				}
			
		}else{
			header('location: '.ROOT_URL.'acounts/admins/');
		}
	}
	
	public function User_delete(){
		$get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
		
		if(isset($get['id'])){
			$this->query('DELETE FROM u0555183_merged.users WHERE id = :id');
			$this->bind('id', $get['id']);
			$this->execute();
			if($this->rowCount() == 1){
					//Redirect
					header('location: '.ROOT_URL.'account/users/');
				}else{
					header('location: '.ROOT_URL.'account/users/');
				}
			
		}else{
			header('location: '.ROOT_URL.'account/users/');
		}
	}
	
	public function User_edit(){
		$get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
		
		if(isset($post['save'])){
			$this->query('UPDATE `u0555183_merged`.users SET nameSurname = :nameSurname, phone = :phone, city = :city, mail = :mail WHERE id = :id');
			$this->bind('nameSurname', $post['nameSurname']);
			$this->bind('phone', $post['phone']);
			$this->bind('city', $post['city']);
			$this->bind('mail', $post['mail']);
			$this->bind('id', $get['id']);
			$this->execute();
			header('location:'.ROOT_URL.'account/users/');
		}
		
		if(isset($get['id'])){
			$this->query('SELECT * FROM `u0555183_merged`.users WHERE id = :id');
			$this->bind('id', $get['id']);
			$row = $this->single();
			return $row;
		}
	}
	
	public function User_block(){
		$get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
		
		$block = explode('-', $get['id']);
		
		$this->query('UPDATE `u0555183_merged`.users SET block = :block WHERE id = :id');
		$this->bind('id', $block[0]);
		$this->bind('block', $block[1]);
		$this->execute();
		header('location:'.ROOT_URL.'account/users/');
		
	}
	
	
	############# Pagination #############
    public function Pagination($values){
        $data = '';
        $per_page = 100;
        $total_values = count($values);

        if(isset($_GET['id'])){
            $current_page = $_GET['id'];
        }else{
            $current_page = 1;
        }
        $counts = ceil($total_values / $per_page);
        $param1 = ($current_page - 1) * $per_page;

        $this->data = array_slice($values, $param1, $per_page);


        for($x=1; $x<=$counts; $x++){
            $numbers[] = $x;
        }

        return $numbers;
    }
	
	public function selectUsersID(){
        $this->query('SELECT id FROM `u0555183_merged`.users');
        $rows = $this->resultSet();
        return $rows;
    }

    public function Logout(){
        unset($_SESSION['admin_data']);
        unset($_SESSION['admin_logged_in']);
        header('location:'.ROOT_URL);
    }
	
}

?>