<?php
class WordsModel extends Model{
	public function replaces_city(){
//		$this->query('SELECT * FROM city');
//		$rows = $this->resultSet();
//
//
//		foreach($rows as $item){
//			$this->query('UPDATE city SET city = :city WHERE id = :id');
//			$this->bind('city', strtoupper($item['city']));
//			$this->bind('id', strtoupper($item['id']));
//			$this->execute();
//		}
		
		//return $rows;
	}
	
	public function Index(){
		$get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
		
		if(isset($post['add-trans'])){			
			$this->query('INSERT INTO `u0555183_merged`.translations (shorten, Trans, LangID) VALUES (:shorten, :trans, "1")');
			$this->bind('shorten', $post['shorten']);
			$this->bind('trans', $post['trans']);
			$this->execute();
			$lastId = $this->lastInsertID();
			if($lastId){
			    echo  $lastId;

                $this->query('UPDATE `u0555183_merged`.translations SET shorten = :shorten WHERE id = :id');
                $this->bind('shorten', $post['shorten'].'_'.$lastId);
                $this->bind('id', $lastId);
                $this->execute();;

				Messages::setMsg('Новое слово добавлено !', 'success');
				header("Location:".ROOT_URL."words/", true, 303);
			}else{
				Messages::setMsg('При добавление произошла ошибка !', 'error');
				header("Location:".ROOT_URL."words/", true, 303);
			}
			
		}

		if(isset($post['search'])){
            $this->query('SELECT * FROM `u0555183_merged`.translations WHERE langID = "1" && (id LIKE "%'.$post['text'].'%" || Trans LIKE "%'.$post['text'].'%" || shorten LIKE "%'.$post['text'].'%") ORDER BY id DESC');
            $rows = $this->resultSet();
            return $rows;
        }
		
		if(!isset($post)){
			$this->query("SELECT * FROM `u0555183_merged`.translations WHERE langID = '1'  ORDER BY id DESC");
			$rows = $this->resultSet();
			return $rows;
		}
	}
	

	
	public function Shorten(){
		$get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
		
		if(isset($post['save'])){
			$this->query('INSERT INTO `u0555183_merged`.translations (shorten, Trans, langID) VALUES (:shorten, :Trans, :langID)');
			$this->bind('shorten', $get['id']);
			$this->bind('Trans', $post['Trans']);
			$this->bind('langID', $post['langID']);
			$this->execute();
			if($this->lastInsertId()){
				header('location:'.ROOT_URL.$get['controller'].'/'.$get['action'].'/'.$get['id'].'/');
			}else{
				Messages::setMsg('Перевод не слова не выполнен :( ', 'error');
			}
		}
		
		$this->query('SELECT * FROM `u0555183_merged`.translations WHERE shorten = :shorten');
		$this->bind('shorten', $get['id']);
		$rows = $this->resultSet();
		return $rows;
	}
	
	public function translateLang(){
		$this->query('SELECT * FROM `u0555183_merged`.languages WHERE id != 1');
		$rows = $this->resultSet();
		return $rows;
	}
	
	public function Edit(){
		$get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
		$ex = explode('~', $get['id']);
		
		if(isset($post['save'])){
			$this->query('UPDATE `u0555183_merged`.translations SET Trans = :Trans WHERE shorten = "'.$ex[0].'" AND langID = "'.$ex[1].'"');
			$this->bind('Trans', $post['trans']);
			$this->execute();
			header('location:'.ROOT_URL.'words/shorten/'.$ex[0]);
		}
		
		
		$this->query('SELECT * FROM `u0555183_merged`.translations WHERE shorten = "'.$ex[0].'" AND langID = "'.$ex[1].'"');
		$row = $this->single();
		return $row;
	}
	
}