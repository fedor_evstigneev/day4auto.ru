<?php
class Account extends Controller{
	protected function Admins(){
        Role::administrator();
        $viewmodel = new AccountModel();
        $this->ReturnView($viewmodel->Admins(), true);

	}
	
	protected function Users(){
        Role::administrator();
        $viewmodel = new AccountModel();
        $this->ReturnView($viewmodel->Users(), true);
	}
	
	protected function Admin_edit(){
		Role::administrator();
        $viewmodel = new AccountModel();
        $this->ReturnView($viewmodel->Admin_edit(), true);
	}
	
	protected function User_edit(){
		Role::administrator();
        $viewmodel = new AccountModel();
        $this->ReturnView($viewmodel->User_edit(), true);

	}
	
	protected function User_block(){
		Role::administrator();
        $viewmodel = new AccountModel();
        $viewmodel->User_block();
	}
	
	protected function Admin_delete(){
		Role::administrator();
        $viewmodel = new AccountModel();
        $this->ReturnView($viewmodel->Admin_delete(), true);
	}
	
	protected function User_delete(){
		Role::administrator();
        $viewmodel = new AccountModel();
        $this->ReturnView($viewmodel->User_delete(), true);
	}

	protected function Logout(){
        Role::administrator();
        $viewmodel = new AccountModel();
        $viewmodel->Logout();
    }
}