<?php
class Accessories extends Controller{
	protected function Index(){
        Role::administrator();
        $viewmodel = new AccessoriesModel();
        $this->ReturnView($viewmodel->Index(), true);
    }

    protected function Add(){
        Role::administrator();
        $viewmodel = new AccessoriesModel();
        $this->ReturnView($viewmodel->Add(), true);
    }

    protected function Edit(){
        Role::administrator();
        $viewmodel = new AccessoriesModel();
        $this->ReturnView($viewmodel->Edit(), true);
    }

    protected function Delete(){
        Role::administrator();
        $viewmodel = new AccessoriesModel();
        $viewmodel->Delete();
    }

    protected function Translate(){
        Role::administrator();
        $viewmodel = new AccessoriesModel();
        $this->ReturnView($viewmodel->Translate(), true);
    }
}