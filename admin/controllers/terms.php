<?php
class Terms extends Controller{
	protected function Index(){
		if(isset($_SESSION['admin_data'])){
			$viewmodel = new TermsModel();
			$this->ReturnView($viewmodel->Index(), true);
		}else{
			header('location:'.ROOT_URL.'terms');
		}
	}
	
	protected function CookiePolicy(){
		if(isset($_SESSION['admin_data'])){
			$viewmodel = new TermsModel();
			$this->ReturnView($viewmodel->CookiePolicy(), true);
		}else{
			header('location:'.ROOT_URL);
		}
	}
}