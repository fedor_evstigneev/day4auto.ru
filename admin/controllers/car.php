<?php
class Car extends Controller{
	protected function Size(){
        Role::administrator();
        $viewmodel = new CarModel();
        $this->ReturnView($viewmodel->Size(), true);
    }

    protected function Door(){
        Role::administrator();
        $viewmodel = new CarModel();
        $this->ReturnView($viewmodel->Door(), true);
    }

    protected function Petrol(){
        Role::administrator();
        $viewmodel = new CarModel();
        $this->ReturnView($viewmodel->petrol(), true);
    }

    protected function Rudder(){
        Role::administrator();
        $viewmodel = new CarModel();
        $this->ReturnView($viewmodel->rudder(), true);
    }

    protected function Run_type(){
        Role::administrator();
        $viewmodel = new CarModel();
        $this->ReturnView($viewmodel->Run_type(), true);
    }

    protected function Airbag(){
        Role::administrator();
        $viewmodel = new CarModel();
        $this->ReturnView($viewmodel->Airbag(), true);
    }

    protected function Accessories(){
        Role::administrator();
        $viewmodel = new CarModel();
        $this->ReturnView($viewmodel->Accessories(), true);
    }

    protected function Gearbox(){
        Role::administrator();
        $viewmodel = new CarModel();
        $this->ReturnView($viewmodel->gearbox(), true);
    }

    protected function Category(){
        Role::administrator();
        $viewmodel = new CarModel();
        $this->ReturnView($viewmodel->Category(), true);
    }

    protected function Cylinder(){
        Role::administrator();
        $viewmodel = new CarModel();
        $this->ReturnView($viewmodel->Cylinder(), true);
    }

//    DELETE details functions

    protected function Size_delete(){
        Role::administrator();
        $viewmodel = new CarModel();
        $viewmodel->Size_delete();
    }

    protected function Run_type_delete(){
        Role::administrator();
        $viewmodel = new CarModel();
        $viewmodel->Run_type_delete();
    }

    protected function Door_delete(){
        Role::administrator();
        $viewmodel = new CarModel();
        $viewmodel->Door_delete();
    }

    protected function Cylinder_delete(){
        Role::administrator();
        $viewmodel = new CarModel();
        $viewmodel->Cylinder_delete();
    }

    protected function Petrol_delete(){
        Role::administrator();
        $viewmodel = new CarModel();
        $viewmodel->Petrol_delete();
    }

    protected function Rudder_delete(){
        Role::administrator();
        $viewmodel = new CarModel();
        $viewmodel->rudder_delete();
    }

    protected function Airbag_delete(){
        Role::administrator();
        $viewmodel = new CarModel();
        $viewmodel->Airbag_delete();
    }

    protected function Accessories_delete(){
        Role::administrator();
        $viewmodel = new CarModel();
        $viewmodel->Accessories_delete();
    }

    protected function Gearbox_delete(){
        Role::administrator();
        $viewmodel = new CarModel();
        $viewmodel->gearbox_delete();
    }

    protected function Category_delete(){
        Role::administrator();
        $viewmodel = new CarModel();
        $viewmodel->Category_delete();
    }

//    Edit details functions
    protected function Size_edit(){
        Role::administrator();
        $viewmodel = new CarModel();
        $this->ReturnView($viewmodel->Size_edit(), true);
    }

    protected function Run_type_edit(){
        Role::administrator();
        $viewmodel = new CarModel();
        $this->ReturnView($viewmodel->Run_type_edit(), true);
    }

    protected function Door_edit(){
        Role::administrator();
        $viewmodel = new CarModel();
        $this->ReturnView($viewmodel->Door_edit(), true);
    }

    protected function Cylinder_edit(){
        Role::administrator();
        $viewmodel = new CarModel();
        $this->ReturnView($viewmodel->Cylinder_edit(), true);
    }

    protected function Petrol_edit(){
        Role::administrator();
        $viewmodel = new CarModel();
        $this->ReturnView($viewmodel->petrol_edit(), true);
    }

    protected function Rudder_edit(){
        Role::administrator();
        $viewmodel = new CarModel();
        $this->ReturnView($viewmodel->rudder_edit(), true);
    }

    protected function Airbag_edit(){
        Role::administrator();
        $viewmodel = new CarModel();
        $this->ReturnView($viewmodel->Airbag_edit(), true);
    }

    protected function Accessories_edit(){
        Role::administrator();
        $viewmodel = new CarModel();
        $this->ReturnView($viewmodel->Accessories_edit(), true);
    }

    protected function Gearbox_edit(){
        Role::administrator();
        $viewmodel = new CarModel();
        $this->ReturnView($viewmodel->gearbox_edit(), true);
    }

    protected function Category_edit(){
        Role::administrator();
        $viewmodel = new CarModel();
        $this->ReturnView($viewmodel->Category_edit(), true);
    }
}