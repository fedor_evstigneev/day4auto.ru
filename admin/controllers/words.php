<?php
class Words extends Controller{
	protected function Index(){
		if(isset($_SESSION['admin_data'])){
			$viewmodel = new WordsModel();
			$this->ReturnView($viewmodel->Index(), true);
		}else{
			header('location:'.ROOT_URL);
		}
	}
	
	protected function Shorten(){
		if(isset($_SESSION['admin_data'])){
			$viewmodel = new WordsModel();
			$this->ReturnView($viewmodel->Shorten(), true);
		}else{
			header('location:'.ROOT_URL);
		}
	}
	
	protected function replaces_city(){
		if(isset($_SESSION['admin_data'])){
			$viewmodel = new WordsModel();
			$this->ReturnView($viewmodel->replaces_city(), true);
		}else{
			header('location:'.ROOT_URL);
		}
	}
	
	protected function Edit(){
		if(isset($_SESSION['admin_data'])){
			$viewmodel = new WordsModel();
			$this->ReturnView($viewmodel->Edit(), true);
		}else{
			header('location:'.ROOT_URL);
		}
	}
}