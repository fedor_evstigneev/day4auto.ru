<?php
class Lang extends Controller{
	protected function Index(){
        Role::administrator();
        $viewmodel = new LangModel();
        $this->ReturnView($viewmodel->Index(), true);
	}

	protected function Del(){
        Role::administrator();
		$viewmodel = new LangModel();
		$viewmodel->Del();
	}

	protected function Add(){
        Role::administrator();
		$viewmodel = new LangModel();
		$this->ReturnView($viewmodel->Add(), true);
	}
}