<?php
class Home extends Controller{
	protected function Index(){
		$viewmodel = new HomeModel();
		$this->ReturnView($viewmodel->Index(), true);
	}

    protected function Delete(){
        Role::administrator();
        $viewmodel = new HomeModel();
        $viewmodel->Delete();
    }

    protected function Off(){
        Role::administrator();
        $viewmodel = new HomeModel();
        $viewmodel->Off();
    }

    protected function On(){
        Role::administrator();
        $viewmodel = new HomeModel();
        $viewmodel->On();
    }

    protected function Vip(){
        Role::administrator();
        $viewmodel = new HomeModel();
        $viewmodel->Vip();
    }

    protected function SuperVip(){
        Role::administrator();
        $viewmodel = new HomeModel();
        $viewmodel->SuperVip();
    }
}