<?php
class Role{
	public static function administrator(){
		if(!isset($_SESSION['admin_logged_in'])){
			Messages::setMsg('Пройдите авторизацию', 'error');
			header('location:'.ROOT_URL);
			exit();
		}
	}
}