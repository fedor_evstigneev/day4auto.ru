<?php
class Bootstrap{
	private $controller;
	private $action;
	private $request;
	
	public function __construct($request){
		//for Default Language
		if(!isset($_SESSION['culture'])){
			$_SESSION['culture'] = 'ru-RU';
		}
		
		$this->request = $request;
		if($this->request['controller'] == ""){
			$this->controller = 'home';
		}else{
			$this->controller = $this->request['controller'];
		}
		if($this->request['action'] == ""){
			$this->action = 'index';
		}else{
			$this->action = $this->request['action'];
		}
	}
	
	public function createController(){
		//check class
		if(class_exists($this->controller)){
			$parents = class_parents($this->controller);
			//check extend
			if(in_array("Controller", $parents)){
				if(method_exists($this->controller, $this->action)){
					return new $this->controller($this->action, $this->request);
				}else{
					//method doesnt exist

	include 'error/404.php';
					return;
				}
			}else{
				//base controller doesnt exists
				echo '<h1>Base controller not found</h1>';
				return;
			}
		}else{
			//Controller class doesnt exists
	include 'error/404.php';

			return;
		}
	}
}