<?php
$langs = new LanguageModel();
$currency = new CurrencyModel();
$query = new Query();
$lang = $langs->SelectLanguage();
$message = new messageModel();
$todayYear = date('Y');
$exception = array('terms', 'contact', 'car', 'my', 'user');
?>

<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="UTF-8">
    <title>Day4auto.ru</title>
    <meta name="keywords" content="kaywords"/>
    <meta name="description" content="Description"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes"/>
    <link rel="stylesheet" href="<?= ROOT_URL . 'assets/css/style.css' ?>">
    <link rel="stylesheet" href="<?= ROOT_URL . 'assets/css/1200.css' ?>">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css?v=1"/>


    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script src="<?= ROOT_URL . 'assets/js/tinymce.js' ?>"></script>


    <!-- Jquery cdn start -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <!-- Bootstrrap cdn start -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>
    <!-- Bootstrrap cdn end -->


    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/css/selectize.bootstrap2.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.4/js/standalone/selectize.min.js"></script>
    <script src="<?= ROOT_URL; ?>assets/js/selectize.js"></script>
    <script src="<?= ROOT_URL; ?>assets/js/actions.js"></script>



    <script src="<?= ROOT_URL; ?>assets/js/car_view.js"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-NMKGZ49992"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-NMKGZ49992');
    </script>
    <meta name="yandex-verification" content="604a77b8757354ef" />


</head>
<body>

<nav class="navbar navbar-expand-lg bg-orange">
    <div class="container">
        <a class="navbar-brand my-brand" href="<?php echo ROOT_PATH; ?>"><img
                    src="<?php echo ROOT_URL; ?>assets/images/day4auto_logo.svg" alt="market logo" height="45"/></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="fa fa-bars"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="<?php echo ROOT_URL; ?>"><?php echo $lang['HOME']; ?></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="<?php echo ROOT_URL; ?>terms/"
                       target="_blank"><?php echo $lang['TERMS']; ?></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="<?php echo ROOT_URL; ?>contact"><?php echo $lang['CONTACT_US']; ?></a>
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo $lang['LANGUAGE']; ?>
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <?php $allLangs = $langs->allLangs(); ?>
                        <?php foreach ($allLangs as $dispLangs): ?>
                            <li>
                                <a href="<?php echo ROOT_URL; ?>language/select/<?php echo $dispLangs['determinant']; ?>">
                                    <img src="<?php echo ROOT_URL . 'assets/images/flag/' . $dispLangs['img']; ?>"
                                         width="20" alt="<?php echo $dispLangs['determinant']; ?>"/>
                                    &nbsp; <?php echo $dispLangs['display_name']; ?>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </li>
                <?php
                if (!isset($_SESSION['user_data'])) {
                    ?>
                    <li>
                        <a id="login" class="hover-color-light" data-toggle="modal" data-target="#loginModal" href=""><?php echo $lang['LOGIN']; ?></a>
                    </li>
                    <li>
                        <a id="registration" class="hover-color-light" data-toggle="modal" data-target="#registrationModal" href=""
                           id="register"><?php echo $lang['REGISTER']; ?></a>
                    </li>
                    <?php
                } else {
                    ?>
                    <li class="dropdown">
                        <a class="dropdown-toggle m-r-0" data-toggle="dropdown"
                           href="#"><?= $_SESSION['user_data']['nameSurname']; ?> <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?= ROOT_URL; ?>my/profile/"><i class="fa fa-user" aria-hidden="true"></i>
                                    Мой профиль</a>
                            </li>
                            <li>
                                <a href="<?= ROOT_URL; ?>my/ads/"><i class="fa fa-camera" aria-hidden="true"></i>
                                    Мои Объявления</a>
                            </li>
                            <?php
                            if (isset($_SESSION['admin_data'])) {
                                ?>
                                <li>
                                    <a id="money" href="<?= ROOT_URL; ?>balance/topup/"><i class="fa fa-credit-card"
                                                                                           aria-hidden="true"></i>
                                        Пополнить Счет | <small><?= $money['money'] . ' Рублей'; ?></small></a>
                                </li>
                                <?php
                            }
                            ?>

                            <li>
                                <a id="sms" href="<?= ROOT_URL; ?>mailbox/messages/">
                                    <i class="fa fa-commenting-o" aria-hidden="true"></i> Сообщения
                                    <span class="label label-danger"><?= $message->notSeenMessages(); ?></span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= ROOT_URL; ?>account/LogOut"><i class="fa fa-sign-out"
                                                                            aria-hidden="true"></i> <?= $lang['LOG_OUT']; ?>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <?php
                }
                ?>
            </ul>
        </div>
    </div>
</nav>

<div class="wrapper m-t-20">
    <div class="container">
        <?php
        Messages::display();
        if((isset($_GET['controller']) && !in_array($_GET['controller'], $exception)) && (isset($_GET['action']) && !in_array($_GET['action'], $exception))) {
            include 'includes/search.php';
        }
        ?>
    </div>

    <?php require($view); ?>
</div>

<footer>
    <div class="footer">
        <h6>
            <?= $lang['FOOTER_TEXT']; ?><br/>
            Day4auto.ru 2020
        </h6>
    </div>
</footer>

<?php
if (!isset($_SESSION['user_data'])) {
    include 'includes/uthentification/resgistration_and_login.php';
}
?>
</body>
</html>