<script src="<?= ROOT_URL; ?>assets/js/preview_image.js"></script>
<?php
$carModel = new CarModel();
?>
<div class="container">
    <form action="" method="post" class="product_form" enctype="multipart/form-data">
        <div class="row">
            <div class="col-sm-8 col-md-6 col-lg-4 col-12 m-b-10">
                <div class="card" class="image_content">
                    <div class="cars_upload"> + Upload an image</div>
                </div>
                <input type="file" name="image[]"  class="shablon" style="display: none;" multiple="multiple">
                <input class="multiple-image" name="image[]" type="file" multiple/>
            </div>
            <div id="all-images" class="col-12">
                <?php
                if(!empty($viewmodel['images'])) {
                    $images = explode(',', $viewmodel['images']);
                }
                if (!empty($images)) {
                    foreach ($images as $key => $image) {
                        if (!empty($image)) {
                            ?>
                            <div class="imagesDiv" id="image_id-<?= $key; ?>" onclick="return confirm('<?= $lang['YOU_ARE_SURE_YOU_WANT_DELETE_THIS_IMAGE']; ?>');">
                                <div style="background: url(<?= ROOT_URL . 'assets/images/cars/mini/' . $image; ?>) no-repeat center center;" class="cover"></div>
                                <div class="imageDeleteButton">
                                    <?= $lang['DELETE']; ?>
                                </div>
                            </div>
                            <?php
                        }
                    }
                }
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <hr/>
            </div>
            <div class="col-sm-4 col-12">
                <div class="form-group">
                    <label for="cars_name"><?= $lang['Manufacturer']; ?></label>
                    <select name="car_name" class="form-control" id="cars_name" required>
                        <?php foreach ($carModel->often_selector('cars_name') as $car) { ?>
                            <option value="<?= $car['id']; ?>"
                                    <?= (isset($viewmodel['cars_name_id']) && $viewmodel['cars_name_id'] == $car['id']) ? 'selected' : '' ?>><?= $car['name']; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="col-sm-4 col-12">
                <div class="form-group">
                    <label for="cars_model"><?= $lang['MODEL']; ?></label>
                    <select name="cars_models" class="form-control" id="cars_model" required>
                        <?php
                        if ((isset($viewmodel['cars_models_id'])) && !empty($viewmodel['cars_models_id']) && (isset($viewmodel['cars_name_id'])) && !empty($viewmodel['cars_name_id']))
                            foreach ($carModel->often_selector('cars_models', 'WHERE cars_name_id = "' . $viewmodel['cars_name_id'] . '"') as $car_models) { ?>
                                <option value="<?= $car_models['id']; ?>"
                                        <?= (isset($viewmodel['cars_models_id']) && $viewmodel['cars_models_id'] == $car_models['id']) ? 'selected' : '' ?>><?= $car_models['name']; ?></option>
                                <?php
                            }
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-sm-4 col-6">
                <div class="form-group">
                    <label for="category"><?= $lang['CATEGORY_TYPE']; ?></label>
                    <select name="category" class="form-control" id="category" required>
                        <?php foreach ($carModel->often_selector('category') as $category) { ?>
                            <option value="<?= $category['id']; ?>"
                                    <?= (isset($viewmodel['category_id']) && $viewmodel['category_id'] == $category['id']) ? 'selected' : '' ?>><?= $lang[$category['name']]; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="col-lg-2 col-sm-3 col-6">
                <div class="form-group">
                    <label for="celinder"><?= $lang['CELINDERS']; ?></label>
                    <select name="celinder" class="form-control" id="celinder" required>
                        <option></option>
                        <?php foreach ($carModel->often_selector('celinder') as $celinder) { ?>
                            <option value="<?= $celinder['id']; ?>"
                                    <?= (isset($viewmodel['celinder_id']) && $viewmodel['celinder_id'] == $celinder['id']) ? 'selected' : '' ?>><?= $celinder['name']; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="col-lg-2 col-sm-3 col-6">
                <div class="form-group">
                    <label for="motor_size"><?= $lang['MOTOR_SIZE']; ?></label>
                    <select name="motor_size" class="form-control" id="motor_size" required>
                        <option></option>
                        <?php foreach ($carModel->often_selector('cars_motor_size') as $motor_size) { ?>
                            <option value="<?= $motor_size['id']; ?>"
                                    <?= (isset($viewmodel['cars_motor_size_id']) && $viewmodel['cars_motor_size_id'] == $motor_size['id']) ? 'selected' : '' ?>><?= $motor_size['name']; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="col-lg-2 col-sm-3 col-6">
                <div class="form-group">
                    <label for="petrol_type"><?= $lang['PETROL_TYPE']; ?></label>
                    <select name="petrol_type" class="form-control" id="petrol_type" required>
                        <option></option>
                        <?php foreach ($carModel->often_selector('cars_petrol_type') as $petrol_type) { ?>
                            <option value="<?= $petrol_type['id']; ?>"
                                    <?= (isset($viewmodel['petrol_type']) && $viewmodel['petrol_type'] == $petrol_type['id']) ? 'selected' : '' ?>><?= $lang[$petrol_type['name']]; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="col-lg-2 col-sm-3 col-6">
                <div class="form-group">
                    <label for="door"><?= $lang['DOORS']; ?></label>
                    <select name="door" class="form-control" id="door" required>
                        <option></option>
                        <?php foreach ($carModel->often_selector('door') as $door) { ?>
                            <option value="<?= $door['id']; ?>"
                                    <?= (isset($viewmodel['door_id']) && $viewmodel['door_id'] == $door['id']) ? 'selected' : '' ?>><?= $door['name']; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="col-lg-2 col-sm-3 col-6">
                <div class="form-group">
                    <label for="rudder"><?= $lang['RUDDER']; ?></label>
                    <select name="rudder" class="form-control" id="rudder" required>
                        <option></option>
                        <?php foreach ($carModel->often_selector('rudder') as $rudder) { ?>
                            <option value="<?= $rudder['id']; ?>"
                                    <?= (isset($viewmodel['rudder']) && $viewmodel['rudder'] == $rudder['id']) ? 'selected' : '' ?>><?= $lang[$rudder['name']]; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="col-lg-2 col-sm-3 col-6">
                <div class="form-group">
                    <label for="year"><?= $lang['YEAR']; ?></label>
                    <select name="year" class="form-control" id="year" required>
                        <option></option>
                        <?php for ($i = $todayYear; $i >= 1900; $i--) { ?>
                            <option class="form-control" value="<?= $i; ?>"
                                    <?= (isset($viewmodel['year']) && $viewmodel['year'] == $i) ? 'selected' : '' ?>><?= $i; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="col-lg-2 col-sm-3 col-6">
                <div class="form-group">
                    <label for="airbag"><?= $lang['AIRBAG']; ?></label>
                    <select name="airbag" class="form-control" id="airbag" required>
                        <option></option>
                        <?php foreach ($carModel->often_selector('airbag') as $airbag) { ?>
                            <option value="<?= $airbag['id']; ?>"
                                    <?= (isset($viewmodel['airbag_id']) && $viewmodel['airbag_id'] == $airbag['id']) ? 'selected' : '' ?>><?= $airbag['name']; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>


            <div class="col-lg-4 col-sm-3 col-6">
                <div class="form-group">
                    <label for="transmission"><?= $lang['TRANSMISSION']; ?></label>
                    <select name="transmission" class="form-control" id="transmission" required>
                        <option></option>
                        <?php foreach ($carModel->often_selector('transmission') as $transmission) { ?>
                            <option value="<?= $transmission['id']; ?>"
                                    <?= (isset($viewmodel['transmission']) && $viewmodel['transmission'] == $transmission['id']) ? 'selected' : '' ?>><?= $lang[$transmission['name']]; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>


            <div class="col-6">
                <div class="form-group">
                    <div class="row">
                        <div class="col-8">
                            <label for="run"><?= $lang['RUN']; ?></label>
                            <input type="number" class="form-control" name="run" id="run" min="0"
                                   value="<?= (isset($viewmodel['run'])) ? $viewmodel['run'] : '' ?>"/>
                        </div>
                        <div class="col-4">
                            <label for="run_type"><?= $lang['RUN_TYPE']; ?></label>
                            <select id="run_type" class="form-control" name="run_type">
                                <?php foreach ($carModel->often_selector('run_type') as $run_type) { ?>
                                    <option value="<?= $run_type['id']; ?>"
                                            <?= (isset($viewmodel['run_type']) && $viewmodel['run_type'] == $run_type['id']) ? 'selected' : '' ?>><?= $lang[$run_type['name']]; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <hr/>
        <div class="row">
            <?php
            (isset($viewmodel['accessories'])) ? $acc = explode(',', $viewmodel['accessories']) : '';

            foreach ($carModel->often_selector('accessories') as $accessories) {
                ?>
                <div class="col-12 col-sm-6 col-lg-3 col-md-4">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="<?= $accessories['id']; ?>"
                               name="accessories[]" id="accessories-<?= $accessories['id']; ?>"
                               <?= (isset($acc) && in_array($accessories['id'], $acc)) ? ' checked' : '' ?>>
                        <label class="form-check-label" for="accessories-<?= $accessories['id']; ?>">
                            <?= $lang[$accessories['name']]; ?>
                        </label>
                    </div>
                </div>
                <?php
            }
            ?>
            <div class="col-12">
                <hr/>
            </div>


            <div class="col-6">
                <div class="form-group">
                    <label for="user_name"><?= $lang['NAME']; ?></label>
                    <input type="text" name="user_name" class="form-control" id="user_name"
                           value="<?= (isset($viewmodel['user_name'])) ? $viewmodel['user_name'] : ''; ?>"/>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="user_phone"><?= $lang['PHONE']; ?></label>
                    <input type="text" name="user_phone" class="form-control" id="user_phone"
                           value="<?= (isset($viewmodel['phone'])) ? $viewmodel['phone'] : ''; ?>"/>
                </div>
            </div>

            <div class="col-6">
                <div class="form-group">
                    <label for="price"><?= $lang['PRICE']; ?></label>
                    <input type="text" name="price" class="form-control" id="price"
                           value="<?= (isset($viewmodel['price'])) ? $viewmodel['price'] : ''; ?>"/>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="city"><?= $lang['CITY']; ?></label>
                    <select id="city" name="city">
                        <?php foreach ($carModel->often_selector('`u0555183_merged`.city') as $city) { ?>
                            <option value="<?= $city['id']; ?>"
                                    <?= (isset($viewmodel['city']) && $viewmodel['city'] == $city['id']) ? 'selected' : '' ?>><?= $lang[$city['city']]; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="col-12">
                <div class="form-group">
                    <label for="description"><?= $lang['DESCRIPTION']; ?></label>
                    <textarea name="description"
                              id="description"><?= (isset($viewmodel['description'])) ? $viewmodel['description'] : '' ?></textarea>
                </div>
            </div>
        </div>
        <div class="form-group">
            <input type="submit" name="save" class="btn btn-success" value="<?= $lang['SAVE']; ?>"/>
        </div>
    </form>
</div>