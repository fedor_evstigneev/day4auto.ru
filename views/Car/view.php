<link rel="stylesheet" href="<?php echo ROOT_URL . 'assets/slider/css/responsiveslides.css'; ?>">
<link rel="stylesheet" href="<?php echo ROOT_URL . 'assets/slider/css/demo.css'; ?>">
<link rel="stylesheet" href="<?php echo ROOT_URL . 'assets/slider/css/style.css'; ?>">
<link rel="stylesheet" href="<?php echo ROOT_URL . 'assets/slider/css/media.css'; ?>">
<script src="<?php echo ROOT_URL . 'assets/slider/js/responsiveslides.min.js'; ?>"></script>
<script src="<?php echo ROOT_URL . 'assets/slider/js/main.js'; ?>"></script>


<?php
$carModel = New CarModel();
//echo '<pre>';
//print_r($viewmodel);
//echo '</pre>';
if (!empty($viewmodel)) {
    $accArray = explode(',', $viewmodel['accessories']);
    ?>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <!-- Slideshow 3 -->
                <ul class="rslides" id="slider3">
                    <?php
                    $images = explode(',', $viewmodel['images']);
                    foreach ($images as $image) {
                    $img = (@fopen(ROOT_URL . 'assets/images/cars/mini/' . $image, "r"))? ROOT_URL . 'assets/images/cars/mini/' .$image:ROOT_URL.'assets/images/no-image.jpg';
                        $getsize = getimagesize($img);
                        if($getsize[0] > $getsize[1]){
                            $size_class = 'slider_image_width';
                        }else{
                            $size_class = 'slider_image_height';
                        }
                        ?>
                        <li class="cursor-pointer">
                            <img class="modal-image <?= $size_class; ?>" id="<?= $img; ?>"
                                 src="<?php echo $img; ?>" alt="">
                        </li>
                        <?php
                    }
                    ?>
                </ul>

                <!-- Slideshow 3 Pager -->
                <ul id="slider3-pager">
                    <?php
                    foreach ($images as $image) {
                        $img = (@fopen(ROOT_URL . 'assets/images/cars/mini/' . $image, "r"))? ROOT_URL . 'assets/images/cars/mini/' .$image:ROOT_URL.'assets/images/no-image.jpg';
                        ?>
                        <li>
                            <a href="#"><img src="<?php echo $img; ?>" alt=""
                                             height="50"></a>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
                <div class="border-1-orange b-t-0 sm-m-b-10">
                    <div class="row">
                        <div class="col-6 user_name"><?= $viewmodel['user_name']; ?></div>
                        <div class="col-6 user_phone"><?= $viewmodel['phone']; ?></div>
                    </div>
                    <?php
                    if ($carModel->allCars($viewmodel['userId']) >= 2) {
                        ?>
                        <div class="b-t-1-ccc">
                            <div class="col-12">
                                <a href="<?= ROOT_URL . 'search/by_user/' . $viewmodel['userId']; ?>"
                                   class="see-all-ads"><?= $lang['SEE_ALL_ADS']; ?></a>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>


            <div class="col-md-6 bg-light-range p-b-5">
                <h1 class="car_name p-t-15"><?= $viewmodel['car_name']; ?> <?= $viewmodel['car_model']; ?></h1>
                <div class="car_info p-10">
                    <div class="row">
                        <div class="col-sm-6 col-12 m-b-10">
                            <img src="<?= SELECT_SHARED_IMAGE . 'icons/calendar.svg'; ?>" class="products-icons-15"
                                 title="<?= $lang['YEAR']; ?>: <?= $viewmodel['year']; ?>"/>
                            <?= $viewmodel['year']; ?>
                        </div>
                        <div class="col-sm-6 col-12 m-b-10">
                            <img src="<?= SELECT_SHARED_IMAGE . 'icons/engine.svg'; ?>" class="products-icons-15"
                                 title="<?= $viewmodel['cars_motor_size_id']; ?> <?= $lang[$viewmodel['petrol_type']]; ?>"/>
                            <?= $viewmodel['cars_motor_size_id']; ?> <?= $lang[$viewmodel['petrol_type']]; ?>
                        </div>
                        <div class="col-sm-6 col-12 m-b-10">
                            <img src="<?= SELECT_SHARED_IMAGE . 'icons/ruble.svg'; ?>" class="products-icons-15"
                                 title="<?= $lang['PRICE']; ?>: <?= $viewmodel['price']; ?>"/>
                            <?= $viewmodel['price']; ?>
                        </div>
                        <div class="col-sm-6 col-12 m-b-10">
                            <img src="<?= SELECT_SHARED_IMAGE . 'icons/run.svg'; ?>" class="products-icons-15"
                                 title="<?= $lang['RUN']; ?> <?= $viewmodel['run_type']; ?>"/>
                            <?= $viewmodel['run']; ?> <?= $lang[$viewmodel['run_type']]; ?>
                        </div>
                        <div class="col-sm-6 col-12 m-b-10">
                            <img src="<?= SELECT_SHARED_IMAGE . 'icons/rudder.svg'; ?>" class="products-icons-15"
                                 title="<?= $lang[$viewmodel['rudder']]; ?>"/>
                            <?= $lang[$viewmodel['rudder']]; ?>
                        </div>
                        <div class="col-sm-6 col-12 m-b-10">
                            <img src="<?= SELECT_SHARED_IMAGE . 'icons/gearbox.svg'; ?>" class="products-icons-15"
                                 title="<?= $lang[$viewmodel['transmission']]; ?>"/>
                            <?= $lang[$viewmodel['transmission']]; ?>
                        </div>
                        <div class="col-sm-6 col-12 m-b-10">
                            <img src="<?= SELECT_SHARED_IMAGE . 'icons/eye.svg'; ?>" class="products-icons-15"
                                 alt="<?= $lang['COUNT_VIEW']; ?>"
                                 title="<?= $lang['COUNT_VIEW']; ?>: <?= $viewmodel['views']; ?>"/>
                            <?= $viewmodel['views']; ?>
                        </div>
                        <div class="col-lg-6 col-12">
                            <img src="<?= SELECT_SHARED_IMAGE . 'icons/location.svg'; ?>" class="products-icons-15"/>
                            <?= $viewmodel['city']; ?>
                        </div>
                    </div>
                </div>
                <h4 class="car_descriotion"><?= $viewmodel['description']; ?></h4>
                <div class="container m-t-10 border-1-orange m-b-10">
                    <div class="row accessories_exist">
                        <div class="col-12">
                            <div class="row">
                                <?php
                                if (!empty($carModel->often_selector('accessories'))) {
                                    foreach ($carModel->often_selector('accessories') as $accessorie) {
                                        ?>
                                        <div class="col-10 col-xl-5"><?= $lang[$accessorie['name']]; ?></div>
                                        <div class="col-2 col-xl-1">
                                            <?= (in_array($accessorie["id"], $accArray)) ? '<i class="fa fa-check check-green" aria-hidden="true"></i>' : '<i class="fa fa-times times-red" aria-hidden="true"></i>'; ?>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="analog-cars m-t-20">
        <div class="container overflow-hidden">
            <h2>Похожие объявления</h2>
            <section class="regular slider">
                <?php
                foreach ($carModel->analog_cars($viewmodel['city']) as $car) {
                    $analogCarImage = explode(',', $car['images']);
                    $analogCarImg = (@fopen(ROOT_URL . 'assets/images/cars/mini/' . $analogCarImage[0], "r"))? ROOT_URL . 'assets/images/cars/mini/' .$analogCarImage[0]:ROOT_URL.'assets/images/no-image.jpg';
                    $analogGetsize = getimagesize($analogCarImg);
                    if($analogGetsize[0] > $analogGetsize[1]){
                        $analogClass = 'analogImg_width';
                    }else{
                        $analogClass = 'analogImg_height';
                    }
                    ?>
                    <div class="border-1-ccc">
                        <a href="<?= ROOT_URL . 'car/view/' . $car['id']; ?>">
                            <div class="analog_ads_image">
                                <img src="<?php echo $analogCarImg; ?>" class="<?= $analogClass; ?>" />
                            </div>
                        </a>
                        <div class="p-1">
                            <div class="row">
                                <div class="col-12 center">
                                    <?= $car['car_name']; ?> <?= $car['car_model']; ?>
                                </div>
                                <div class="col-lg-7 col-12">
                                    <img src="<?= SELECT_SHARED_IMAGE . 'icons/ruble.svg'; ?>" class="products-icons-15"/>
                                    <?= $car['price']; ?>
                                </div>
                                <div class="col-lg-5 col-12">
                                    <img src="<?= SELECT_SHARED_IMAGE . 'icons/calendar.svg'; ?>" class="products-icons-15"/>
                                    <?= $car['year']; ?>
                                </div>
                                <div class="col-12">
                                    <img src="<?= SELECT_SHARED_IMAGE . 'icons/location.svg'; ?>" class="products-icons-15"/>
                                    <?= $car['city']; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </section>
        </div>
    </div>


    <div class="click-modal-image"></div>
    <div class="max-width-image">
        <div class="close"><i class="fa fa-times" aria-hidden="true"></i></div>

        <ul class="rslides image-max-size-ul modal-image" id="slider4">
            <?php
            foreach ($images as $key => $image) {
                $analogCarImg = (@fopen(ROOT_URL . 'assets/images/cars/mini/' . $image, "r"))? ROOT_URL . 'assets/images/cars/mini/' .$image:ROOT_URL.'assets/images/no-image.jpg';
                $getsize = getimagesize($analogCarImg);
                if ($getsize[1] > $getsize[0]) {
                    $prodImgClass = 'prodImageHeight';
                } else {
                    $prodImgClass = 'prodImageWidth';
                }
                ?>
                <li class="prod-image-max-size">
                    <img src="<?= $analogCarImg; ?>" class="<?= $prodImgClass; ?> mob-max-h-280"/>
                </li>
                <?php
            }
            ?>
        </ul>
        <div class="clear"></div>
    </div>


    <!--slick slider-->
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL . 'assets/slick_slider/slick.css'; ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL . 'assets/slick_slider/slick-theme.css'; ?>">
    <script src="<?php echo ROOT_URL . 'assets/slick_slider/slick.js'; ?>" type="text/javascript"
            charset="utf-8"></script>
    <?php
}
?>