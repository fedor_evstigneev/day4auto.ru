<div class="container">
    <a class="add_products m-t-20 m-b-20 p-15" href="<?= ROOT_URL.'car/add/'; ?>">
            <i class="fa fa-plus fa-5x d-block" aria-hidden="true"></i>
            <div class="size"><?= $lang['ADD_ADS']; ?></div>
    </a>
    <?php
    if (!empty($viewmodel)) {
        foreach ($viewmodel as $item) {
            $firstImg = explode(',', $item['images']);
            $imageUrl = ROOT_URL . 'assets/images/cars/mini/' . $firstImg[0];
            $image = '';
            $prodImgClass = '';

            if (!empty($firstImg[0]) && @fopen($imageUrl, "r")) {
                $getsize = getimagesize($imageUrl);
                $image = $imageUrl;
                ($getsize[0] < $getsize[1]) ? $prodImgClass = 'prodImageHeight' : $prodImgClass = 'prodImageWidth';
            } else {
                $image = ROOT_URL . 'assets/images/no-image.svg';
            }
            ?>
            <div itemscope itemtype="http://schema.org/Product" class="products">
                <div class="row">
                    <div class="col-lg-3 col-md-5">
                        <div class="all-products-images-div">
                            <img src="<?php echo $image; ?>" class="<?php echo $prodImgClass; ?>" itemprop="image"
                                 alt="<?php echo $item['name']; ?>"/>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-7">
                        <a href="<?php echo ROOT_URL . 'car/view/' . $item['id']; ?>" class="product-url">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="h2-product-name" itemprop="name"><?= $item['name']; ?> </h2>
                                    <div class="row" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                                        <div class="col-6 col-md-4 col-lg-3">
                                            <img src="<?= SELECT_SHARED_IMAGE . 'icons/calendar.svg'; ?>"
                                                 class="products-icons-15"/>
                                            <span><?= $item['year']; ?> <?= $lang['YEAR']; ?></span>
                                        </div>
                                        <div class="col-6 col-md-4 col-lg-3">
                                            <img src="<?= SELECT_SHARED_IMAGE . 'icons/engine.svg'; ?>"
                                                 class="products-icons-15"/>
                                            <span><?= $item['cars_motor_size_id']; ?> <?= $lang[$item['petrol_type']]; ?></span>
                                        </div>
                                        <div class="col-6 col-md-4 col-lg-3">
                                            <span>
                                                <img src="<?= SELECT_SHARED_IMAGE . 'icons/run.svg'; ?>"
                                                     class="products-icons-15"/>
                                                <?= $item['run']; ?> <?= $item['run_type']; ?></span>
                                        </div>
                                        <div class="col-6 col-md-4 col-lg-3">
                                            <img src="<?= SELECT_SHARED_IMAGE . 'icons/rudder.svg'; ?>"
                                                 class="products-icons-15"/>
                                            <span><?= $lang[$item['rudder']]; ?></span>
                                        </div>
                                        <div class="col-6 col-md-4 col-lg-3">
                                            <span>
                                                <img src="<?= SELECT_SHARED_IMAGE . 'icons/gearbox.svg'; ?>"
                                                     class="products-icons-15"/>
                                                <span><?= $lang[$item['transmission']]; ?></span>
                                            </span>
                                        </div>
                                        <div class="col-6 col-md-4 col-lg-3">
                                            <span itemprop="priceCurrency" content="RUB">
                                                <img src="<?= SELECT_SHARED_IMAGE . 'icons/ruble.svg'; ?>"
                                                     class="products-icons-15"/>
                                                <span itemprop="price"
                                                      content="<?= $item['price']; ?>"><?= $item['price']; ?></span>
                                            </span>
                                        </div>
                                        <div class="col-sm-12 m-t-15 description" itemprop="description">
                                            <?php
                                            $length = 290;
                                            $string = strip_tags($item['description']);
                                            if (strlen($string) > $length) {
                                                $stringCut = substr($string, 0, $length);
                                                $string = substr($stringCut, 0, strrpos($stringCut, ' ')) . ' ...';
                                            }
                                            echo $string;
                                            ?>
                                        </div>
                                    </div>
                                </div>

                                <h5 class="product-date">
                                    <label class="label label-warning product-date-label">06-02-2020</label><br>
                                </h5>
                            </div>
                        </a>
                    </div>
                    <div class="col-12">
                        <hr/>
                    </div>
                    <div class="col-12">
                        <a href="<?= ROOT_URL . 'car/edit/' . $item['id']; ?>" class="btn btn-warning btn-sm">
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            <?= $lang['EDIT']; ?>
                        </a>
                        <a href="<?= ROOT_URL . 'car/action/' . $item['id']; ?>"
                           class="btn <?= ($item['on_off'] == 'on') ? 'btn-success' : 'btn-secondary'; ?> btn-sm">
                            <i class="fa fa-toggle-on" aria-hidden="true"></i>
                            <?= ($item['on_off'] == 'on') ? $lang['ON'] : $lang['OFF']; ?>
                        </a>
                        <a href="<?= ROOT_URL . 'car/delete/' . $item['id']; ?>" onclick="return confirm('Вы уверины что хотите удалить это объявление ?');" class="btn btn-danger btn-sm">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                            <?= $lang['DELETE']; ?>
                        </a>
                    </div>
                </div>
            </div>
            <?php
        }
    }
    ?>
</div>
