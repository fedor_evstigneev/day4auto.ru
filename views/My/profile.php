<?php
$my = new MyModel();
?>
<div class="container">
    <div class="row personal-info">
        <div class="col-md-6">
            <div class="card">
                <div class="card" id="image_content">
                    <?php
                    if (!empty($viewmodel['image'])) {
                        ?>
                        <div class="personal-image">
                            <div class="hover-delete"><?= $lang['DELETE']; ?></div>
                            <img src="<?= SELECT_SHARED_IMAGE . 'user/' . $viewmodel['image']; ?>" class="card-img"/>
                        </div>
                        <?php
                    } else {
                        echo '<div id="upload_image"> + Upload an image</div>';
                    }
                    ?>
                </div>
                <input id="image" type="file">
                <p class="m-t-15">Change password:</p>
                <div class="card">
                    <form action="" method="post">
                        <div class="row">
                            <div class="col-6"><label for="current_password">Текущий пароль:</label></div>
                            <div class="col-6">
                                <div class="form-group">
                                    <input type="password" name="current_password" id="current_password"
                                           class="form-control"
                                           value="<?= (isset($_POST['current_password'])) ? $_POST['current_password'] : ''; ?>"
                                           required/>
                                </div>
                            </div>
                            <div class="col-6"><label for="new_password">Новый пароль:</label></div>
                            <div class="col-6">
                                <div class="form-group">
                                    <input type="password" name="new_password" id="new_password" class="form-control"
                                           value="<?= (isset($_POST['new_password'])) ? $_POST['new_password'] : ''; ?>"
                                           required/>
                                </div>
                            </div>
                            <div class="col-6"><label for="repeat_password">Повторите пароль:</label></div>
                            <div class="col-6">
                                <div class="form-group">
                                    <input type="password" name="repeat_password" id="repeat_password"
                                           class="form-control"
                                           value="<?= (isset($_POST['repeat_password'])) ? $_POST['repeat_password'] : ''; ?>"
                                           required/>
                                </div>
                            </div>
                            <div class="col-12">
                                <input type="submit" name="update_password" class="btn btn-yelow float-right"
                                       value="Изменить пароль"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6 second-block">
            <div class="card">
                <p>Личные данные:</p>
                <form action="" method="post">
                    <div class="row">
                        <div class="col-5"><label for="name_surname"><?= $lang['NAME_SURNAME']; ?>:</label></div>
                        <div class="col-7">
                            <div class="form-group">
                                <input type="text" name="name_surname" id="name_surname" class="form-control"
                                       value="<?= $viewmodel['nameSurname'] ?>" required/>
                            </div>
                        </div>
                        <div class="col-5"><label for="email"><?= $lang['EMAIL']; ?>:</label></div>
                        <div class="col-7">
                            <div class="form-group">
                                <input type="email" name="email" id="email" class="form-control"
                                       value="<?= $viewmodel['mail'] ?>" required/>
                            </div>
                        </div>
                        <div class="col-5"><label for="phone"><?= $lang['PHONE']; ?>:</label></div>
                        <div class="col-7">
                            <div class="form-group">
                                <input type="tel" name="phone" pattern="[+]{1}[0-9]{9,11}" required id="phone"
                                       class="form-control" value="<?= $viewmodel['phone'] ?>"/>
                            </div>
                        </div>

                        <div class="col-5"><label for="city">Город:</label></div>
                        <div class="col-7">
                            <div class="form-group">
                                <select name="city" id="city">
                                    <?php
                                    if (count($my->city()) >= 1) {
                                        foreach ($my->city() as $city) {
                                            ?>
                                            <option value="<?php echo $city['city']; ?>"
                                                    <?= ($viewmodel['city'] == $city['city']) ? 'selected' : ''; ?>><?php echo $lang[$city['city']]; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <input type="submit" name="save_change" class="btn btn-yelow float-right"
                                   value="Сохранить изменения"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


