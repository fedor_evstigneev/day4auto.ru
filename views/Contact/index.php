<div class="container">
    <div class="row">
        <div class="col-12 col-md-4 m-t-20">
            <form action="" method="POST" class="border p-15">
                <div class="form-group">
                    <label for="name"><?= $lang['NAME']; ?> :</label>
                    <input type="text" name="name" id="name" class="form-control" REQUIRED/>
                </div>
                <div class="form-group">
                    <label for="mail"><?= $lang['EMAIL']; ?> :</label>
                    <input type="email" name="mail" id="mail" class="form-control" REQUIRED/>
                </div>

                <div class="form-group">
                    <label for="subject"><?= $lang['SUBJECT']; ?> :</label>
                    <input type="text" name="subject" id="subject" class="form-control" REQUIRED/>
                </div>
                <div class="form-group">
                    <label for="text"><?= $lang['TEXT']; ?> :</label>
                    <textarea class="ContactTextarea" name="text" id="text" REQUIRED></textarea>
                </div>
                <div class="form-group">
                    <div class="g-recaptcha" data-sitekey="6LdLvjwUAAAAAEUQsnSDgOOjmyKU5-i0ait4kDmp"></div>
                </div>
                <div class="form-group">
                    <input type="submit" name="send" class="btn btn-success" value="<?= $lang['SEND']; ?>"/>
                </div>
            </form>
        </div>

        <div class="col-12 col-md-8 m-t-20 contact-map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d33196351.91199918!2d57.16114038406075!3d59.480686947979216!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46962f237058ddb7%3A0x1e9f009a8254ccd1!2sday4shop!5e0!3m2!1sru!2sge!4v1582130873878!5m2!1sru!2sge"
                    width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        </div>
    </div>
</div>