$(document).ready(function () {
    $('.imagesDiv').click(function () {

        var idName = $(this).attr("id");
        $.ajax({
            type: "POST",
            data: {
                delete_image: idName,
            },
            success: function (data) {
                if (data) {
                    console.log(data)
                    $('div').remove('#' + idName);
                }
            }
        });
    })


    // Upload Images Start
    $(document).on("click", "#cars_upload", function () {
        document.querySelector("#cars_images").click();
    });


    $(document).on("change", "#cars_images", function () {
        //on change event
        formdata = new FormData();

        formdata.append('action', 'uploadImages');
        jQuery.each($("input[name^='image']")[0].files, function (i, file) {
            formdata.append('image[' + i + ']', file);
        });

        jQuery.ajax({
            type: "POST",
            data: formdata,
            processData: false,
            contentType: false,
            success: function (data) {
                data = JSON.parse(data);
                console.log(data)
                var image = data;
                each(image, function (key, value) {
                    $("#all-images").html('<div class="personal-image"><div class="hover-delete">Удалить</div><img src=' + image + ' class="card-img" alt=""></div>');
                });
            }
        });
    });

    // Upload Images End


    $('#cars_name').change(function (e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            // url: "home/index",
            data: {
                cars_name: $('#cars_name').val(),
            },
            success: function (data) {
                if (data) {
                    var parse_data = JSON.parse(data);
                    $("#cars_model").empty();
                    $("#cars_model").append('<option value=""></option>');
                    $.each(parse_data, function (index, value) {
                        $("#cars_model").append('<option value="'+value['id']+'">'+value['name']+'</option>');
                    });
                }
            }
        });
    });

    $('.cars_upload').click(function () {
        document.querySelector(".multiple-image").click();
        var _this = $(this);
        var form = $('.product_form');
        var i = $('.shablon').clone().removeClass('shablon').addClass('multiple-image').show();
        form.prepend(i);
        // _this.hide();

    });

});


