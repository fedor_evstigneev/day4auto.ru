$(document).on("change",".multiple-image",function () {
    var _this = $(this);
    var form = $('.product_form');

    var fileList = this.files;
    var anyWindow = window.URL || window.webkitURL;

    for(var i = 0; i < fileList.length; i++){
        var objectUrl = anyWindow.createObjectURL(fileList[i]);
        $('#all-images').append('<div class="imagesDiv"><div class="cover" style="background: url(' + objectUrl + ') no-repeat center center; background-size: 100%;"></div><div class="imageDeleteButton">Удалить</div></div>');
        window.URL.revokeObjectURL(fileList[i]);
    }
});