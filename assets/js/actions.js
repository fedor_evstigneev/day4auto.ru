$(document).ready(function () {
    // $('.city').click(function (e) {
    //     var $select =  $( this ).selectize();
    //     var selectize = $select[0].selectize;
    //     e.preventDefault();
    //     $.ajax({
    //         type: "POST",
    //         url: "home/index",
    //         data: {
    //             import_city: 'city',
    //         },
    //         success: function (data) {
    //             if (data) {
    //                 selectize.open();
    //                 var dataArr = JSON.parse(data);
    //                 dataArr.forEach(function(city){
    //                     selectize.addOption({value:city.city,text:city.city});
    //                 });
    //                 $('div').removeClass('city');
    //             }
    //         }
    //     });
    // });

    $('.car_name').change(function (e) {
        var $select =  $('select#car_models.car_models.selectized').selectize();
        var selectize = $select[0].selectize;
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "home/index",
            data: {
                import_cars_models_id: $('#car_name').val(),
            },
            success: function (data) {
                console.log(data)
                if (data) {
                    var dataArr = JSON.parse(data);
                    $(selectize.options).remove();
                    dataArr.forEach(function(cars_models){
                        selectize.addOption({value:cars_models.id,text:cars_models.name});
                    });
                }
            }
        });
    });


    // Upload Images Start
    $(document).on("click","#upload_image",function () {
        document.querySelector("#image").click();
    });


    $(document).on("change","#image",function () {
        //on change event
        formdata = new FormData();
        if ($(this).prop('files').length > 0) {
            file = $(this).prop('files')[0];
            formdata.append("image", file);
        }

        jQuery.ajax({
            type: "POST",
            // url: "my/profile/",
            data: formdata,
            processData: false,
            contentType: false,
            success: function (data) {
                data=JSON.parse(data);
                var image=data;
                $("#image_content").html('<div class="personal-image"><div class="hover-delete">Удалить</div><img src='+image+' class="card-img" alt=""></div>');


            }
        });
    });

    // Upload Images End

    $(document).on("click",".hover-delete",function () {
        $.ajax({
            type: "POST",
            url: "my/profile/",
            data: {
                delete_profile_image: 'image',
            },
            success: function (data) {
                if (data) {
                    $("#image_content").empty();
                    $("#image_content").html('<div id="upload_image"> + Upload an image</div>');
                }
            }
        });
    });

});
