tinymce.init({
    selector: 'textarea',
    height: 200,
    theme: 'modern',
    fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
    plugins: [
        'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        'searchreplace wordcount visualblocks visualchars code fullscreen',
        'insertdatetime media nonbreaking save table contextmenu directionality',
        'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
    ],
    image_advtab: true,
    templates: [{
        title: 'Test template 1',
        content: 'Test 1'
    },
        {
            title: 'Test template 2',
            content: 'Test 2'
        }
    ],
    content_css: [
        '<?php echo ROOT_PATH; ?>assets/css/style.css',
        '//www.tinymce.com/css/codepen.min.css'
    ],
    verify_html: false,
    mode : "textareas",
    force_br_newlines : false,
    force_p_newlines : false,
    forced_root_block : ''
});
