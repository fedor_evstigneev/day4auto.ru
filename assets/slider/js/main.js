// You can also use "$(window).load(function() {"
$(function () {
    // Slideshow 3
    $("#slider3").responsiveSlides({
        manualControls: '#slider3-pager',
        maxwidth: 540
    });
});


$(document).ready(function(){
    $('.modal-image').click(function(){
        $(".click-modal-image").css("display", "block");
        $(".max-width-image").css("display", "block");
    });

    $('.fa-times').click(function(){
        $(".click-modal-image").css("display", "none");
        $(".max-width-image").css("display", "none");
    });

    $('.click-modal-image').click(function(){
        $(".max-width-image").css("display", "none");
        $(".click-modal-image").css("display", "none");
    });
});

var screenWidth = window.screen.width;
var screenHeight = window.screen.height;

var differenceScreenWidth = screenWidth / screenHeight;

if(screenWidth >= screenHeight){
    if(screenWidth >= 1000){
        widthForImg = 800;
    }else{
        widthForImg = screenWidth - 100;
    }
}


// You can also use "$(window).load(function() {"
$(function () {
    // Slideshow 4
    $("#slider4").responsiveSlides({
        auto: false,
        pager: false,
        nav: true,
        speed: 500,
        namespace: "callbacks",
        before: function () {
            $('.events').append("<li>before event fired.</li>");
        },
        after: function () {
            $('.events').append("<li>after event fired.</li>");
        }
    });

});
