<?php
class Account extends Controller{
	protected function logOut(){
        unset($_SESSION['is_logged_in']);
        unset($_SESSION['user_data']);
        header('location:'.$_SERVER['HTTP_REFERER']);
	}
}