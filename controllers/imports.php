<?php
class Imports extends Controller{
	public function cities(){
		$viewmodel = new ImportsModel();
		$this->ReturnView($viewmodel->cities(), true);
	}

    protected function cars_models(){
        $viewmodel = new ImportsModel();
        $this->ReturnView($viewmodel->cars_models(), true);
    }
}