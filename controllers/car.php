<?php
class Car extends Controller{
	protected function View(){
		$viewmodel = new CarModel();
		$this->ReturnView($viewmodel->View(), true);
	}

    protected function Delete(){
        Role::user();
        $viewmodel = new CarModel();
        $viewmodel->Delete();
    }

    protected function Edit(){
        Role::user();
        $viewmodel = new CarModel();
        $this->ReturnView($viewmodel->Edit(), true);
    }

    protected function Add(){
        Role::user();
        $viewmodel = new CarModel();
        $this->ReturnView($viewmodel->Add(), true);
    }

    protected function Action(){
        Role::user();
        $viewmodel = new CarModel();
        $viewmodel->Action();
    }
}