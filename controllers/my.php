<?php
class My extends Controller{
	protected function Profile(){
	    Role::user();
		$viewmodel = new MyModel();
		$this->ReturnView($viewmodel->Profile(), true);
	}

    protected function Ads(){
        Role::user();
        $viewmodel = new MyModel();
        $this->ReturnView($viewmodel->Ads(), true);
    }
}