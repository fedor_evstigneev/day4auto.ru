<form action="<?php echo ROOT_URL; ?>" method="post" class="car_search m-b-10">
    <div class="row">
        <div class="col-6 col-sm-3">
            <div class="form-group">
                <select id="car_name" class="car_name" name="car_name" placeholder="Марка авто">
                    <option value="NULL">Марка авто</option>
                    <?php
                    foreach ($query->select('cars_name', "ORDER BY name ASC") as $car_name){
                        ?>
                        <option value="<?= $car_name['id'] ?>" <?= (isset($_GET['car_name']) && $_GET['car_name'] == $car_name['id'])? 'selected':'' ?>>
                            <?= $car_name['name'] ?>
                        </option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="col-6 col-sm-3">
            <div class="form-group">
                <select id="car_models" name="car_models"  class="car_models" placeholder="Модель">
                <?php
                    if(isset($_GET['car_name']) && $_GET['car_name'] !== ''){
                        foreach($query->select('cars_models', ' WHERE cars_name_id = '.$_GET['car_name']) as $car_model){
                        ?>
                        <option value="<?= $car_model['id'] ?>" <?= (isset($_GET['car_models']) && $_GET['car_models'] == $car_model['id'])?'selected':''; ?>>
                            <?= $car_model['name']; ?>
                        </option>
                        <?php
                        }
                    }
                ?>
                </select>
            </div>
        </div>
        <div class="col-6 col-sm-3">
            <div class="form-group">
                <select id="car_category" class="car_category" name="car_category" placeholder="Категория">
                    <option value="NULL">Категория</option>
                    <?php
                    foreach ($query->select('category', "ORDER BY name ASC") as $category){
                        ?>
                        <option value="<?= $category['id'] ?>" <?= (isset($_GET['car_category']) && $_GET['car_category'] == $category['id'])? 'selected':'' ?>><?= $category['name'] ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="col-6 col-sm-4 col-md-3">
            <div class="form-group">
                <select name="rudder" placeholder="Руль" class="form-control">
                    <option value="NULL">Руль</option>
                    <?php
                    foreach ($query->select('rudder', "") as $rudder){
                        ?>
                        <option value="<?= $rudder['id'] ?>"<?= (isset($_GET['rudder']) && $_GET['rudder'] == $rudder['id'])? 'selected':'' ?>><?= $lang[$rudder['name']] ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3">
            <div class="form-group">
                <select name="motor_size_start" class="form-control">
                    <option value="NULL">Объём двигателя От</option>
                    <?php
                    foreach ($query->select('cars_motor_size', "ORDER BY id ASC") as $motor_size){
                        ?>
                        <option value="<?= $motor_size['id'] ?>" <?= (isset($_GET['motor_size_start']) && $_GET['motor_size_start'] == $motor_size['id'])? 'selected':'' ?>><?= $motor_size['name'] ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="col-6 col-sm-4 col-md-3">
            <div class="form-group">
                <select name="motor_size_to" class="form-control">
                    <option value="NULL">Объём двигателя До</option>
                    <?php
                    foreach ($query->select('cars_motor_size', "ORDER BY id DESC") as $motor_size){
                        ?>
                        <option value="<?= $motor_size['id'] ?>" <?= (isset($_GET['motor_size_to']) && $_GET['motor_size_to'] == $motor_size['id'])? 'selected':'' ?>><?= $motor_size['name'] ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>

        <div class="col-6 col-sm-3 col-md-2">
            <div class="form-group">
                <select id="year_from" name="year_from">
                    <option value=""><?php echo $lang['YEAR']; ?><?php echo $lang['FROM']; ?></option>
                    <?php
                    for ($i = $todayYear; $i >= 1930; $i--) {
                        ?>
                        <option class="form-control" value="<?= $i; ?>" <?= (isset($_GET['year_from']) && $_GET['year_from'] == $i)? 'selected':'' ?>><?= $i; ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="col-6 col-sm-3 col-md-2">
            <div class="form-group">
                <select id="year_to" name="year_to">
                    <option value=""><?php echo $lang['YEAR']; ?> до</option>
                    <?php
                    for ($i = $todayYear; $i >= 1930; $i--) {
                        ?>
                        <option class="form-control" <?= (isset($_GET['year_to']) && $_GET['year_to'] == $i)? 'selected':'' ?> value="<?= $i; ?>"><?= $i; ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>

        <div class="col-6 col-sm-4 col-md-2">
            <div class="form-group">
                <select name="fuel" class="form-control">
                    <option value=""><?= $lang['PETROL_TYPE']; ?></option>
                    <?php
                    foreach ($query->select('cars_petrol_type', "") as $petrol){
                        ?>
                        <option <?= (isset($_GET['fuel']) && $_GET['fuel'] == $petrol['id'])? 'selected':'' ?> value="<?=$petrol['id'] ?>"><?= $lang[$petrol['name']] ?></option>';
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="col-6 col-sm-4 col-md-3">
            <div class="form-group">
                <select name="transmission" class="form-control">
                    <option value="">Трансмиссия</option>
                    <?php
                    foreach ($query->select('transmission', "") as $transmission){
                        ?>
                        <option <?= (isset($_GET['transmission']) && $_GET['transmission'] == $transmission['id'])?'selected':'' ?> value="<?= $transmission['id'] ?>"><?= $lang[$transmission['name']] ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>

        <div class="col-6 col-sm-3 col-md-2">
            <div class="form-group">
                <input type="number" class="form-control" name="price_from"
                       placeholder="<?php echo $lang['PRICE']; ?> <?php echo $lang['FROM']; ?>" value="<?= (isset($_GET['price_from']))?$_GET['price_from']:'' ?>"/>
            </div>
        </div>
        <div class="col-6 col-sm-3 col-md-2">
            <div class="form-group">
                <input type="number" class="form-control" name="price_to"
                       placeholder="<?php echo $lang['PRICE']; ?> до" value="<?= (isset($_GET['price_to']))?$_GET['price_to']:'' ?>"/>
            </div>
        </div>
        <div class="col-6 col-sm-3 col-md-2">
            <div class="form-group">
                <select name="doors" class="form-control">
                    <option value=""><?= $lang['DOORS']; ?></option>
                    <?php
                    foreach ($query->select('door', "") as $door){
                        ?>
                        <option <?= (isset($_GET['doors']) && $_GET['doors'] == $door['id'])?'selected':'' ?> value="<?= $door['id'] ?>"><?= $door['name'] ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="col-6 col-sm-3">
            <div class="form-group">
                <select name="city" id="city" class="city">
                    <option value="NULL"><?php echo $lang['CITY']; ?></option>
                    <?php
                    foreach ($query->select('`u0555183_merged`.city', "WHERE hide = 0 ORDER BY number DESC, city ASC") as $city){
                        ?>
                        <option value="<?= $city['id'] ?>" <?= (isset($_GET['city']) && $_GET['city'] == $city['id'])?'selected':'' ?>><?= $lang[$city['city']] ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>
    </div>
    <input type="submit" name="search" value="<?php echo $lang['SEARCH']; ?>" class="btn btn-orange"/>
</form>