<!-- Login modal start  -->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <b><?= $lang['LOGIN']; ?></b>
                <button type="button" class="close m-r-0-important" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?php echo ROOT_PATH; ?>" method="post" id="login_form">
                    <div class="form-group">
                        <label for="mail"><?php echo $lang['EMAIL']; ?></label>
                        <input type="email" name="mail" id="mail" class="form-control" required value="<?php echo (isset($_POST['mail']) ? $_POST['mail'] : ''); ?>" />
                    </div>
                    <div class="form-group">
                        <label for="password"><?php echo $lang['PASSWORD']; ?></label>
                        <input type="password" name="password" id="password" class="form-control" value="<?php echo (isset($_POST['password']) ? $_POST['password'] : ''); ?>" required />
                    </div>
                    <input type="submit" name="sign_in" id="sign_in" class="btn btn-success" value="<?php echo $lang['SIGN_IN']; ?>"/>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo $lang['CLOSE']; ?></button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Login modal end  -->

<!-- Registration modal start  -->
<div class="modal fade" id="registrationModal" tabindex="-1" role="dialog" aria-labelledby="registrationModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <b><?= $lang['REGISTER']; ?></b>
                <button type="button" class="close m-r-0-important" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="post" id="registration_form">
                    <div class="form-group">
                        <label for="name_surname"><?php echo $lang['NAME_SURNAME']; ?></label>
                        <input type="text" name="name_surname" id="name_surname" class="form-control" required value="<?php echo (isset($_POST['name_surname']) ? $_POST['name_surname'] : ''); ?>" />
                    </div>
                    <div class="form-group">
                        <label for="register_mail"><?php echo $lang['EMAIL']; ?></label>
                        <input type="mail" name="register_mail" id="register_mail" class="form-control" required value="<?php echo (isset($_POST['register_mail']) ? $_POST['register_mail'] : ''); ?>" />
                    </div>
                    <div class="form-group">
                        <label for="RegCountry"><?= $lang['CITY']; ?>:</label>
                        <select id="RegCountry" name="city" REQUIRED>
                            <option><?php echo $lang['CITY']; ?></option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="phone"><?php echo $lang['PHONE']; ?></label>
                        <input type="text" name="phone" id="phone" class="form-control" required value="<?php echo (isset($_POST['phone']) ? $_POST['phone'] : ''); ?>" />
                    </div>
                    <div class="form-group">
                        <label for="register_password"><?php echo $lang['PASSWORD']; ?></label>
                        <input type="password" name="register_password" id="register_password" class="form-control" required value="<?php echo (isset($_POST['register_password']) ? $_POST['register_password'] : ''); ?>" />
                    </div>
                    <div class="form-group">
                        <label for="confirm_password"><?php echo $lang['CONFIRM_PASSWORD']; ?></label>
                        <input type="password" name="confirm_password" id="confirm_password" class="form-control" required value="<?php echo (isset($_POST['confirm_password']) ? $_POST['confirm_password'] : ''); ?>" />
                    </div>
                    <input type="submit" class="btn btn-success" value="<?php echo $lang['REGISTER']; ?>"/>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo $lang['CLOSE']; ?></button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Registration modal end  -->