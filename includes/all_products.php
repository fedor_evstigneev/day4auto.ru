<div class="container">
    <?php
    if (!empty($viewmodel)) {
        foreach ($viewmodel as $item) {
            $firstImg = explode(',', $item['images']);
            $image = '';
            $prodImgClass = '';

            if (!empty($firstImg[0]) && @fopen(ROOT_URL . 'assets/images/cars/mini/' . $firstImg[0], "r")) {
                $image = ROOT_URL . 'assets/images/cars/mini/' . $firstImg[0];
            } else {
                $image = ROOT_URL . 'assets/images/no-image.jpg';
            }
            $getsize = getimagesize($image);
            ($getsize[0] < $getsize[1]) ? $prodImgClass = 'prodImageHeight' : $prodImgClass = 'prodImageWidth';
            ?>
            <div itemscope itemtype="http://schema.org/Product" class="products">
                <a href="<?php echo ROOT_URL . 'car/view/' . $item['id']; ?>" class="product-url">
                    <div class="row">
                        <div class="col-lg-3 col-md-5">
                            <div class="all-products-images-div <?= $prodImgClass; ?>">
                                <img src="<?php echo $image; ?>" itemprop="image"
                                     alt="<?php echo $item['car_name'] . ' ' . $item['price']; ?>"/>
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-7">
                            <div class="row">
                                <div class="col-12">
                                    <h2 class="h2-product-name"
                                        itemprop="name"><?= $item['car_name']; ?> <?= $item['car_model']; ?></h2>
                                    <div class="row" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                                        <div class="col-6 col-md-4 col-lg-3">
                                            <img src="<?= SELECT_SHARED_IMAGE . 'icons/calendar.svg'; ?>"
                                                 class="products-icons-15"/>
                                            <span><?= $item['year']; ?> <?= $lang['YEAR']; ?></span>
                                        </div>
                                        <div class="col-6 col-md-4 col-lg-3">
                                            <img src="<?= SELECT_SHARED_IMAGE . 'icons/engine.svg'; ?>"
                                                 class="products-icons-15"/>
                                            <span><?= $item['cars_motor_size_id']; ?> <?= $lang[$item['petrol_type']]; ?></span>
                                        </div>
                                        <div class="col-6 col-md-4 col-lg-3">
                                            <span>
                                                <img src="<?= SELECT_SHARED_IMAGE . 'icons/run.svg'; ?>"
                                                     class="products-icons-15"/>
                                                <?= $item['run']; ?> <?= $lang[$item['run_type']]; ?></span>
                                        </div>
                                        <div class="col-6 col-md-4 col-lg-3">
                                            <img src="<?= SELECT_SHARED_IMAGE . 'icons/rudder.svg'; ?>"
                                                 class="products-icons-15"/>
                                            <span><?= $lang[$item['rudder']]; ?></span>
                                        </div>
                                        <div class="col-6 col-md-4 col-lg-3">
                                            <span>
                                                <img src="<?= SELECT_SHARED_IMAGE . 'icons/gearbox.svg'; ?>"
                                                     class="products-icons-15"/>
                                                <span><?= $lang[$item['transmission']]; ?></span>
                                            </span>
                                        </div>
                                        <div class="col-6 col-md-4 col-lg-3">
                                            <span itemprop="priceCurrency" content="RUB">
                                                <img src="<?= SELECT_SHARED_IMAGE . 'icons/ruble.svg'; ?>"
                                                     class="products-icons-15"/>
                                                <span itemprop="price"
                                                      content="<?= $item['price']; ?>"><?= $item['price']; ?></span>
                                            </span>
                                        </div>
                                        <div class="col-12 col-md-4 col-lg-3">
                                            <img src="<?= SELECT_SHARED_IMAGE . 'icons/location.svg'; ?>"
                                                 class="products-icons-15"/>
                                            <?= $item['city']; ?>
                                        </div>
                                        <div class="col-sm-12 m-t-15 description" itemprop="description">
                                            <?php
                                            $length = 290;
                                            $string = strip_tags($item['description']);
                                            if (strlen($string) > $length) {
                                                $stringCut = substr($string, 0, $length);
                                                $string = substr($stringCut, 0, strrpos($stringCut, ' ')) . ' ...';
                                            }
                                            echo $string;
                                            ?>
                                        </div>
                                    </div>
                                </div>

                                <h5 class="product-date">
                                    <label class="label label-warning product-date-label">06-02-2020</label><br>
                                </h5>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <?php
        }
    }
    ?>
</div>
