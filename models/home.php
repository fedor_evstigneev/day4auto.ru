<?php

class HomeModel extends Model
{
    private $per_page = 15;

    public function Index()
    {
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        if(isset($post)) {
            if(isset($post['search'])){
                foreach($post as $key => $item){
                    if(isset($create_get)){
                        if (!empty($item)) {
                            $create_get .= '&'.$key . '=' . $item;
                        }else{
                            $create_get .= '&'.$key . '=NULL';
                        }
                    }else{
                        if (!empty($item)) {
                            $create_get = $key . '=' . $item;
                        }else{
                            $create_get = $key . '=NULL';
                        }
                    }
                }
                header('location:'.ROOT_URL.'search/auto/page&'.$create_get);
            }
            if(isset($post['import_city']) && $post['import_city'] == 'city'){
                $this->import_cities();
                die();
            }

            if(isset($post['import_cars_models_id']) && $post['import_cars_models_id'] != ''){
                $this->import_cars_models();
                die();
            }

            ############## REGISTRATION AND SIGN UP START ####################
            if (isset($post['name_surname']) && isset($post['register_mail']) && isset($post['city']) && isset($post['phone']) && isset($post['register_password']) && isset($post['confirm_password'])) {
                $this->registration();
            }

            if (isset($post['mail']) && isset($post['password']) && isset($post['sign_in'])) {
                $this->sign_up();
            }
            ############## REGISTRATION AND SIGN UP END ####################
        }

        if(!empty($get['id']) AND is_numeric($get['id'])){$current_page = $get['id'];}else{$current_page = 1;}

        if(!isset($_SESSION['select_priority_products'])){$_SESSION['select_priority_products'] = 'id DESC';}
        if(isset($post['select_priority_products'])){
            if($post['select_priority_products'] == 'news'){$_SESSION['select_priority_products'] = "id DESC";}
            if($post['select_priority_products'] == 'lower_price_first'){$_SESSION['select_priority_products'] = "price ASC";}
            if($post['select_priority_products'] == 'highes_price_first'){$_SESSION['select_priority_products'] = "price DESC";}
        }

        $param1 = ($current_page - 1) * $this->per_page;
        $this->query(
            'SELECT cars_name.name as car_name, cars_models.name as car_model, cars.id, cars.images, cars.views, cars.price, cars.year, cars.description, cars.date, 
                    cars.accessories, cars.run, cars.user_name, cars.phone, 
                    cars_motor_size.name as cars_motor_size_id, cars_petrol_type.name as petrol_type, rudder.name as rudder, transmission.name as transmission, `u0555183_merged`.`city`.`city` as city,
                    run_type.name as run_type
                    FROM cars
                    LEFT JOIN cars_name ON cars.cars_name_id = cars_name.id
                    LEFT JOIN run_type ON cars.run_type = run_type.id
                    LEFT JOIN `u0555183_merged`.`city` ON cars.city = city.id
                    LEFT JOIN cars_models ON cars.cars_models_id = cars_models.id
                    LEFT JOIN cars_motor_size ON cars.cars_motor_size_id = cars_motor_size.id
                    LEFT JOIN cars_petrol_type ON cars.petrol_type = cars_petrol_type.id
                    LEFT JOIN rudder ON cars.rudder = rudder.id
                    LEFT JOIN transmission ON cars.transmission = transmission.id
                    WHERE on_off = "on" ORDER BY id DESC LIMIT :param1, :per_page');
        $this->bind('param1', $param1);
        $this->bind('per_page', $this->per_page);
        $rows = $this->resultSet();
        return $rows;
    }

    ################# LOGIN START #################
    public function sign_up()
    {
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        if (filter_var($post['mail'], FILTER_VALIDATE_EMAIL)) {
            $this->query('SELECT * FROM u0555183_merged.users WHERE mail = :mail && password = :password');
            $this->bind('mail', $post['mail']);
            $this->bind('password', MD5($post['password']));
            $rows = $this->single();
            if (!empty($rows) >= 1) {
                if ($rows['block'] != 1) {
                    $_SESSION['is_logged_in'] = true;
                    $_SESSION['user_data'] = array(
                        "id" => $rows['id'],
                        "nameSurname" => $rows['nameSurname'],
                        "mail" => $rows['mail'],
                        "city" => $rows['city'],
                        "phone" => $rows['phone'],
                        "status" => $rows['status']
                    );
                    header('location:'.$_SERVER['HTTP_REFERER']);
                } else {
                    Messages::setMsg('Ваш аккаунт заблокирован за нарушение правил !', 'error');
                    unset($_SESSION['is_logged_in']);
                    unset($_SESSION['user_data']);
                }
            } else {
                Messages::setMsg('Неверный логин или пароль', 'error');
            }
        }
    }
    ################# LOGIN END   #################

    ################# REGISTRATION START #################
    public function registration(){
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        $this->query('SELECT * FROM u0555183_merged.users WHERE mail = :mail');
        $this->bind('mail', $post['register_mail']);
        $row = $this->single();
        if(empty($row)){
            if(strlen($post['register_password']) >= 4 && strlen($post['confirm_password']) >= 4){
                if($post['register_password'] == $post['confirm_password']){
                    $password = md5($post['register_password']);
                    $this->query('INSERT INTO u0555183_merged.users (nameSurname, mail, city, phone, password, status) VALUES (:nameSurname, :mail, "'.$_POST['city'].'", :phone, :password, :status)');
                    $this->bind('nameSurname', $post['name_surname']);
                    $this->bind('mail', $post['register_mail']);
                    $this->bind('phone', $post['phone']);
                    $this->bind('password', $password);
                    $this->bind('status', 4);
                    $this->execute();
                    if($this->lastInsertId()){
                        $_SESSION['is_logged_in'] = true;
                        $_SESSION['user_data'] = array(
                            "id"		=>$this->lastInsertId(),
                            "nameSurname"	=>$post['name_surname'],
                            "mail"			=>$post['register_mail'],
                            "city"			=>$post['city'],
                            "phone"			=>$post['phone'],
                            "status"		=>4);
                        Messages::setMsg('Регистрация успешно завершена', 'error');
                        header('location:'.$_SERVER['HTTP_REFERER']);
                    }
                }else{
                    Messages::setMsg('Пароли не совпадают', 'error');
                }
            }else{
//                Messages::setMsg('Пароль должен содержать как минимум 4 символа', 'error');
                Messages::setMsg('Пароль должен содержать как минимум 4 символа', 'error');
            }
        }else {
            Messages::setMsg('Аккаунт с такой Эл. почтой был зарегистрирован ранее', 'error');
        }
    }
    ################# REGISTRATION END   #################


    ## Pagination ##
    public function Pagination($values){
        $data = '';
        $per_page = 15;
        $total_values = count($values);

        if(!empty($_GET['id'])){$current_page = $_GET['id'];}else{$current_page = 1;}
        $counts = ceil($total_values / $per_page);
        $param1 = ($current_page - 1) * $per_page;
        $this->data = array_slice($values, $param1, $per_page);

        for($x=1; $x<=$counts; $x++){
            $numbers[] = $x;
        }
        return $numbers;
    }


    public function import_cities(){
        include 'includes/import/cities.php';
    }

    public function import_cars_models(){
        include 'includes/import/cars_models.php';
    }

}