<?php
class CarModel extends Model{
	public function View(){
		$get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);

        $this->query('SELECT cars_name.name as car_name, cars.userId, cars_models.name as car_model, cars.id, cars.images, cars.views, cars.price, cars.year, cars.description, cars.date, 
                            cars.accessories, cars.run, cars.user_name, cars.phone, 
                            cars_motor_size.name as cars_motor_size_id, cars_petrol_type.name as petrol_type, rudder.name as rudder, transmission.name as transmission, `u0555183_merged`.`city`.`city` as city,
                            run_type.name as run_type
                            FROM cars
                            LEFT JOIN cars_name ON cars.cars_name_id = cars_name.id
                            LEFT JOIN run_type ON cars.run_type = run_type.id
                            LEFT JOIN `u0555183_merged`.`city` ON cars.city = city.id
                            LEFT JOIN cars_models ON cars.cars_models_id = cars_models.id
                            LEFT JOIN cars_motor_size ON cars.cars_motor_size_id = cars_motor_size.id
                            LEFT JOIN cars_petrol_type ON cars.petrol_type = cars_petrol_type.id
                            LEFT JOIN rudder ON cars.rudder = rudder.id
                            LEFT JOIN transmission ON cars.transmission = transmission.id
                            WHERE cars.id = :id && (cars.on_off = "on" || cars.userId = :test)
                            GROUP BY cars.id');
        $this->bind('id', $get['id']);
        $this->bind('test', (isset($_SESSION['user_data']['id']))?$_SESSION['user_data']['id']:'');
        $row = $this->single();
        if(!$row){
            header('location:'.ROOT_URL.'error/404.php/');
            die();
        }
        return $row;
	}

	public function allCars($id){
	    $this->query('SELECT id FROM cars WHERE userId = "'.$id.'"');
	    $rows = $this->resultSet();
	    return count($rows);
    }

    public function analog_cars($city){
        $get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
        $this->query('SELECT cars_name.name as car_name, cars.userId, cars_models.name as car_model, cars.id, cars.images, cars.views, cars.price, cars.year, cars.description, cars.date, 
                            cars.accessories, cars.run, cars.user_name, cars.phone, 
                            cars_motor_size.name as cars_motor_size_id, cars_petrol_type.name as petrol_type, rudder.name as rudder, transmission.name as transmission, `u0555183_merged`.`city`.`city` as city,
                            run_type.name as run_type
                            FROM cars
                            LEFT JOIN cars_name ON cars.cars_name_id = cars_name.id
                            LEFT JOIN run_type ON cars.run_type = run_type.id
                            LEFT JOIN `u0555183_merged`.`city` ON cars.city = city.id
                            LEFT JOIN cars_models ON cars.cars_models_id = cars_models.id
                            LEFT JOIN cars_motor_size ON cars.cars_motor_size_id = cars_motor_size.id
                            LEFT JOIN cars_petrol_type ON cars.petrol_type = cars_petrol_type.id
                            LEFT JOIN rudder ON cars.rudder = rudder.id
                            LEFT JOIN transmission ON cars.transmission = transmission.id
                            WHERE cars.id != "'.$get['id'].'" && cars.on_off = "on" ORDER BY FIELD(cars.city, "'.$city.'") DESC, vipStatus DESC LIMIT 15');
	    $rows = $this->resultSet();
	    return $rows;
    }

    public function Delete(){
        $get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);

        $this->query('SELECT images FROM cars WHERE id = :id && userId = "'.$_SESSION['user_data']['id'].'"');
        $this->bind('id', $get['id']);
        $row = $this->single();
        $ex = explode(',', $row['images']);

        foreach($ex as $image){
            unlink('assets/images/cars/'.$image);
            unlink('assets/images/cars/mini/'.$image);
        }

        $this->query('DELETE FROM cars WHERE id = :id && userId = "'.$_SESSION['user_data']['id'].'"');
        $this->bind('id', $get['id']);
        $this->execute();
        if($this->rowCount()){
            header('location:'.ROOT_URL.'my/ads');
        }else{
            header('location:'.ROOT_URL);
        }
    }
    public function Action(){
        $get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
        $this->query('UPDATE cars SET on_off = IF(on_off = "on", "off", "on") WHERE id = :id && userId = "'.$_SESSION['user_data']['id'].'" ');
        $this->bind('id', $get['id']);
        $this->execute();
        if(isset($_SERVER['HTTP_REFERER'])){
            $backUrl = $_SERVER['HTTP_REFERER'];
        }else{
            $backUrl = ROOT_URL.'my/ads/';
        }
        header('location:'.$backUrl);
    }

    public function Edit(){
        $get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        $this->query(
            'SELECT cars.*, cars.id, cars.images, cars.name, cars.price, cars.city, cars.year, cars.description, cars.date, 
                    cars.accessories, cars.run, cars.run_type, cars.user_name, cars.phone, cars.cars_motor_size_id, cars.petrol_type, cars.rudder, cars.transmission
                    FROM cars
                    WHERE id = "'.$get['id'].'" && userId = "'.$_SESSION['user_data']['id'].'"');
        $row = $this->single();

        if(isset($post['cars_name'])){
            echo json_encode($this->select_cars_models($post['cars_name']));
            die();
        }
        if(isset($post['save'])){
            $this->edit_car($get['id'], $row['images']);
        }

        if(isset($post['delete_image'])){
            $images = explode(',', $row['images']);
            $deleteImage = str_replace('image_id-','', $post['delete_image']);

            $deletingImage = '';
            foreach($images as $key => $image){
                if($deleteImage == $key){
                    echo $image;
                    $deletingImage = $image;
                }else{
                    $sortingImages[] = $image;
                }
            }

            unlink("assets/images/cars/".$deletingImage);
            unlink("assets/images/cars/mini/".$deletingImage);

            $doneImg = implode(',', $sortingImages);
            $this->query('UPDATE cars SET images = :doneImg WHERE id = "'.$row['id'].'"');
            $this->bind('doneImg', $doneImg);
            $this->execute();
            die();
        }
        if(!$row){
            header('location:'.ROOT_URL.'error/404.php/');
            die();
        }
        return $row;
    }

    public function Add(){
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        if(isset($post['cars_name'])){
            echo json_encode($this->select_cars_models($post['cars_name']));
            die();
        }

        if(isset($post['save'])){
//            echo '<pre>';
//            print_r($post);
//            echo '<pre>';
            $this->add_car();
        }



    }

    public function add_car(){
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        $images = $this->ImgProcessor();

        $this->query('INSERT INTO cars 
        (images, cars_name_id, cars_models_id, category_id, cars_motor_size_id, door_id, transmission, rudder, price, city, year, description, accessories, airbag_id, celinder_id, run, run_type, petrol_type, user_name, phone, userId, on_off, date)
        VALUES
        ("'.$images.'", :cars_name_id, :cars_models_id, :category, :cars_motors_size_id, :door_id, :transmission, :rudder, :price, :city, :year, :description, :accessories, :airbag_id, :celinder_id, :run, :run_type, :petrol_type, :user_name, :phone, :userId, "on", :date)');

        $this->bind('cars_name_id', $post['car_name']);
        $this->bind('cars_models_id', $post['cars_models']);
        $this->bind('category', $post['category']);
        $this->bind('celinder_id', $post['celinder']);
        $this->bind('cars_motors_size_id', $post['motor_size']);
        $this->bind('transmission', $post['transmission']);
        $this->bind('petrol_type', $post['petrol_type']);
        $this->bind('door_id', $post['door']);
        $this->bind('rudder', $post['rudder']);
        $this->bind('year', $post['year']);
        $this->bind('airbag_id', $post['airbag']);
        $this->bind('run', $post['run']);
        $this->bind('run_type', $post['run_type']);
        $this->bind('accessories', (isset($post['accessories']))? implode(',', $post['accessories']): '');

        $this->bind('user_name', $post['user_name']);
        $this->bind('userId', $_SESSION['user_data']['id']);
        $this->bind('phone', $post['user_phone']);
        $this->bind('description', $post['description']);

        $this->bind('price', $post['price']);
        $this->bind('city', $post['city']);
        $this->bind('date', date("Y-m-d"));
        $this->execute();
        if($this->lastInsertId()){
            header('location:'.ROOT_URL.'my/ads/');
        }else{
            Messages::setMsg('Incorrect Login', 'error');
        }
    }

    public function edit_car($id, $oldImages = ''){
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        $newImages = $this->ImgProcessor();
        if(!empty($oldImages)){
            $images = $oldImages.','.$newImages;
        }else{
            $images = $newImages;
        }

        $this->query('UPDATE cars SET images = "'.$images.'", cars_name_id = :cars_name_id, cars_models_id = :cars_models_id, category_id = :category, cars_motor_size_id = :cars_motors_size_id, transmission = :transmission, rudder = :rudder, price = :price, city = :city,
                            `year` = :year, description = :description, accessories = :accessories, run = :run, run_type = :run_type, petrol_type = :petrol_type, user_name = :user_name, phone = :phone,
                             celinder_id = :celinder_id, door_id = :door_id, airbag_id = :airbag_id
                             WHERE userId = :userId && id = :id
        ');

        $this->bind('cars_name_id', $post['car_name']);
        $this->bind('cars_models_id', $post['cars_models']);
        $this->bind('category', $post['category']);
        $this->bind('celinder_id', $post['celinder']);
        $this->bind('cars_motors_size_id', $post['motor_size']);
        $this->bind('transmission', $post['transmission']);
        $this->bind('petrol_type', $post['petrol_type']);
        $this->bind('door_id', $post['door']);
        $this->bind('rudder', $post['rudder']);
        $this->bind('year', $post['year']);
        $this->bind('airbag_id', $post['airbag']);
        $this->bind('run', $post['run']);
        $this->bind('run_type', $post['run_type']);
        $this->bind('accessories', (isset($post['accessories']))? implode(',', $post['accessories']): '');

        $this->bind('user_name', $post['user_name']);
        $this->bind('userId', $_SESSION['user_data']['id']);
        $this->bind('phone', $post['user_phone']);
        $this->bind('description', $post['description']);

        $this->bind('price', $post['price']);
        $this->bind('city', $post['city']);

        $this->bind('id', $id);
        $this->execute();
        header('location: '.ROOT_URL.'my/ads/');
    }

    // სურათების პროცესორი
    public $imageNamesSum;
    public $imageNamesSum2;
    public function ImgProcessor(){
        $i = 0;
        $imageNamesSum = '';
        foreach($_FILES['image']['tmp_name'] as $key => $tmp_name ){
            if(!empty($tmp_name)) {
                //ვქმნით ფაილის მონაცემებს
                $file = $_FILES['image']['name'][$key];
                $tmp = $_FILES['image']['tmp_name'][$key];
                $filesize = $_FILES['image']['size'][$key];
                $size = floor($filesize / 1024);
                $ext = pathinfo($file, PATHINFO_EXTENSION);
                $ext = strtolower($ext);

                if ($ext == "jpg" || $ext == "jpeg") {
                    $src = imagecreatefromjpeg($tmp);
                } elseif ($ext == "png") {
                    $src = imagecreatefrompng($tmp);
                } else {
                    $src = imagecreatefromgif($tmp);
                }

                list($width, $height) = getimagesize($tmp);

                if ($width > $height) {
                    $new_width = 1000;
                    $new_height = ($height / $width) * $new_width;
                } else {
                    $new_width = 1000;
                    $new_height = ($height / $width) * $new_width;
                }

                list($width2, $height2) = getimagesize($tmp);

                if ($width2 > $height2) {
                    $new_width2 = 200;
                    $new_height2 = ($height2 / $width2) * $new_width2;
                } else {
                    $new_height2 = 200;
                    $new_width2 = ($width2 / $height2) * $new_height2;
                }


                $resize = imagecreatetruecolor($new_width, $new_height);
                imagecopyresampled($resize, $src, 0, 0, 0, 0, $new_width, $new_height, $width, $height);

                $resize2 = imagecreatetruecolor($new_width2, $new_height2);
                imagecopyresampled($resize2, $src, 0, 0, 0, 0, $new_width2, $new_height2, $width2, $height2);

                $dir = 'assets/images/cars/';
                $dir2 = 'assets/images/cars/mini/';
                $filename = time() . MD5($_FILES["image"]["name"][$key]) . '.' . $ext;
                $full = $dir . $filename;
                $full2 = $dir2 . $filename;

                imagejpeg($resize, $full, 80);
                imagejpeg($resize2, $full2, 80);
                /*
                echo '<strong>Orig File:</strong> '.$file.'<br />';
                echo '<strong>Tmp Name:</strong> '.$tmp.'<br />';
                echo '<strong>Extension:</strong> '.$ext.'<br />';
                echo '<strong>Width x Height:</strong> '.$width.' X '.$height.'<br />';
                echo '<strong>New Width x Height:</strong> '.$new_width.' X '.$new_height.'<br />';
                echo '<strong>Size:</strong> '.$size.'<br />';
                echo '<strong>New File Name:</strong> '.$full.'<br />';
                echo '<br /><img src="'.$full.'" /><br /><br />';
                */
                $imageNames[] = $filename;
                $imageNamesSum = implode(',', $imageNames);
                $i++;
            }
        }
        return $imageNamesSum;
    }

    public function select_cars_models($id = ''){
        $this->query('SELECT * FROM cars_models WHERE cars_name_id = "'.$id.'"');
        $models = $this->resultSet();
        return $models;
    }

    public function often_selector($table = '', $where = ''){
        $this->query('SELECT * FROM '.$table.' '.$where);
        $result = $this->resultSet();
        return $result;
    }
}