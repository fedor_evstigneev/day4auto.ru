<?php

class ImportsModel extends Model
{
    public function cities(){
        $this->query('SELECT city FROM city  ORDER BY number DESC, city ASC');
        $rows = $this->resultSet();
        return $rows;
    }

    public function cars_models(){
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        $this->query('SELECT id, name FROM cars_models WHERE cars_name_id = "'.$post['import_cars_models_id'].'" ORDER BY id ASC');
        $rows = $this->resultSet();
        return $rows;
    }
}