<?php
class CurrencyModel extends Model{
	
	public function defaultSymbol(){
		if(!isset($_SESSION['currency']) OR !isset($_SESSION['symbol'])){
			$_SESSION['currency'] = 'GEL';
			$_SESSION['symbol'] = 'ლ';
		}
	}
	
	public function select(){
		$get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
		
		if(isset($get['id'])){
			$_SESSION['currency'] = $get['id'];
		}elseif(isset($_SESSION['currency'])){
			
		}else{
			$_SESSION['currency'] = 'GEL';
			$_SESSION['symbol'] = 'ლ';
		}

		$this->query('SELECT * FROM currency WHERE name = :name');
		$this->bind(':name', $_SESSION['currency']);
		$currencyRows = $this->resultSet();
		
		foreach($currencyRows as $curr){
			$currency = $curr['name'];
			$symbol = $curr['symbol'];
		}
		if(isset($_GET['id'])){
			header('Location: ' . $_SERVER['HTTP_REFERER']);
		}
		$_SESSION['currency'] = $currency;
		$_SESSION['symbol'] = $symbol;
	}
	
	public function allCurrencies(){
		$this->query('SELECT * FROM currency');
		$currency = $this->resultSet();
		return $currency;
	}
	
	public function dispCurrency($name){
		$this->query('SELECT * FROM currency WHERE name = :name');
		$this->bind(':name', $name);
		$currency = $this->single();
		return $currency['symbol'];
	}
	
	public function GetCurrency($curr){
		if($curr == 'USD'){
			$currency = '2.40';
		}elseif($curr == 'EUR'){
			$currency = '2.60';
		}else{
			$currency = '1';
		}
		return $currency;
	}
	
	public function convert($convertible, $curr){
		//$client = new SoapClient('http://nbg.gov.ge/currency.wsdl');
		return round($convertible / $this->GetCurrency($curr), 2);
	}
	
	//convert back for price filters
	public function convertBack($convertible, $curr){
		//$client = new SoapClient('http://nbg.gov.ge/currency.wsdl');
		$back = $convertible * $this->GetCurrency($curr);
		return $back;
	}

}