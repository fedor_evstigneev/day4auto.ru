<?php

class SearchModel extends Model
{
    private $per_page = 15;
    public function By_user(){
        $get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
        if(empty($get['id'])){header('location:'.ROOT_URL);}

        if(!empty($get['page']) AND is_numeric($get['page'])){$current_page = $get['page'];}else{$current_page = 1;}

        $param1 = ($current_page - 1) * $this->per_page;
        $this->query(
            'SELECT cars_name.name as car_name, cars_models.name as car_model, cars.id, cars.images, cars.views, cars.price, cars.year, cars.description, cars.date, 
                    cars.accessories, cars.run, cars.user_name, cars.phone, 
                    cars_motor_size.name as cars_motor_size_id, cars_petrol_type.name as petrol_type, rudder.name as rudder, transmission.name as transmission, `u0555183_merged`.`city`.`city` as city,
                    run_type.name as run_type
                    FROM cars
                    LEFT JOIN cars_name ON cars.cars_name_id = cars_name.id
                    LEFT JOIN run_type ON cars.run_type = run_type.id
                    LEFT JOIN `u0555183_merged`.`city` ON cars.city = city.id
                    LEFT JOIN cars_models ON cars.cars_models_id = cars_models.id
                    LEFT JOIN cars_motor_size ON cars.cars_motor_size_id = cars_motor_size.id
                    LEFT JOIN cars_petrol_type ON cars.petrol_type = cars_petrol_type.id
                    LEFT JOIN rudder ON cars.rudder = rudder.id
                    LEFT JOIN transmission ON cars.transmission = transmission.id
                    WHERE on_off = "on" && userId = :userId ORDER BY id DESC LIMIT :param1, :per_page');
        $this->bind('param1', $param1);
        $this->bind('userId', $get['id']);
        $this->bind('per_page', $this->per_page);
        $rows = $this->resultSet();
        if(!$rows){
            header('location:'.ROOT_URL.'error/404.php/');
            die();
        }
        return $rows;
    }

    public function Auto(){
        $get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);

        $i = 0;
        foreach($get as $key => $search_get){
            if($key != 'controller' && $key != 'action' && $key != 'id' && $key != 'page' && $key != 'search') {
                $getArray[] = $search_get;
            }
            $i++;
        }

        $where = 'WHERE ';
        $conditionArray[] = 'cars.on_off = "on"';
        if(isset($getArray[0]) && $getArray[0] !== '' && $getArray[0] !== 'NULL'){$conditionArray[] = ' cars.cars_name_id = '.$getArray[0];}
        if(isset($getArray[1]) && $getArray[1] !== '' && $getArray[1] !== 'NULL'){$conditionArray[] = ' cars.cars_models_id = '.$getArray[1];}
        if(isset($getArray[2]) && $getArray[2] !== '' && $getArray[2] !== 'NULL'){$conditionArray[] = ' cars.category_id = '.$getArray[2];}
        if(isset($getArray[3]) && $getArray[3] !== '' && $getArray[3] !== 'NULL'){$conditionArray[] = ' cars.rudder = '.$getArray[3];}
        if(isset($getArray[4]) && $getArray[4] !== '' && $getArray[4] !== 'NULL'){$conditionArray[] = ' cars.cars_motor_size_id >= '.$getArray[4];}
        if(isset($getArray[5]) && $getArray[5] !== '' && $getArray[5] !== 'NULL'){$conditionArray[] = ' cars.cars_motor_size_id <= '.$getArray[5];}
        if(isset($getArray[6]) && $getArray[6] !== '' && $getArray[6] !== 'NULL'){$conditionArray[] = 'cars. year >= '.$getArray[6];}
        if(isset($getArray[7]) && $getArray[7] !== '' && $getArray[7] !== 'NULL'){$conditionArray[] = ' cars.year <= '.$getArray[7];}
        if(isset($getArray[8]) && $getArray[8] !== '' && $getArray[8] !== 'NULL'){$conditionArray[] = ' cars.petrol_type = '.$getArray[8];}
        if(isset($getArray[9]) && $getArray[9] !== '' && $getArray[9] !== 'NULL'){$conditionArray[] = ' cars.transmission = '.$getArray[9];}
        if(isset($getArray[10]) && $getArray[10] !== '' && $getArray[10] !== 'NULL'){$conditionArray[] = ' cars.price >= '.$getArray[10];}
        if(isset($getArray[11]) && $getArray[11] !== '' && $getArray[11] !== 'NULL'){$conditionArray[] = ' cars.price <= '.$getArray[11];}
        if(isset($getArray[12]) && $getArray[12] !== '' && $getArray[12] !== 'NULL'){$conditionArray[] = ' cars.door_id = '.$getArray[12];}
        if(isset($getArray[13]) && $getArray[13] !== '' && $getArray[13] !== 'NULL'){$conditionArray[] = ' cars.city = '.$getArray[13];}

        $join = implode(' && ', $conditionArray);
        $condition = $where.' '.$join;
        $this->query('SELECT cars_name.name as car_name, cars_models.name as car_model, cars.*,
                            cars_motor_size.name as cars_motor_size_id, cars_petrol_type.name as petrol_type, rudder.name as rudder, transmission.name as transmission, `u0555183_merged`.`city`.`city` as city,
                            run_type.name as run_type
                            FROM cars 
                            LEFT JOIN cars_name ON cars.cars_name_id = cars_name.id
                            LEFT JOIN run_type ON cars.run_type = run_type.id
                            LEFT JOIN `u0555183_merged`.`city` ON cars.city = city.id
                            LEFT JOIN cars_models ON cars.cars_models_id = cars_models.id
                            LEFT JOIN cars_motor_size ON cars.cars_motor_size_id = cars_motor_size.id
                            LEFT JOIN cars_petrol_type ON cars.petrol_type = cars_petrol_type.id
                            LEFT JOIN rudder ON cars.rudder = rudder.id
                            LEFT JOIN transmission ON cars.transmission = transmission.id
                            '.$condition);
        $rows = $this->resultSet();
        if(!$rows){
            Messages::setMsg('Объявления не найдены!', 'error');
        }
        return $rows;
    }
}