<?php
class MyModel extends Model{
    private $per_page = 15;

	public function Profile(){
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

	    if(isset($post['update_password'])){
	        $this->changePassword();
        }

	    if(isset($post['save_change'])){
	        $this->saveChange();
        }

	    if(!empty($_FILES)){
            $imageName = $this->ImgProcessor();
            $this->saveImageInDB($imageName);
        }

	    if(isset($post['delete_profile_image'])){
	        $this->deleteProfileImage();
	        die();
        }

        $this->query('SELECT * FROM `u0555183_merged`.users WHERE id = "'.$_SESSION['user_data']['id'].'"');
        $row = $this->single();
        return $row;
	}

	public function Ads(){
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        if(!empty($get['id']) AND is_numeric($get['id'])){$current_page = $get['id'];}else{$current_page = 1;}

        $param1 = ($current_page - 1) * $this->per_page;
        $this->query(
            'SELECT cars.id, cars.images, cars.on_off, cars.name, cars.price, cars.city, cars.year, cars.description, cars.date, 
                    cars.accessories, cars.run, cars.run_type, cars.user_name, cars.phone, 
                    cars_motor_size.name as cars_motor_size_id, cars_petrol_type.name as petrol_type, rudder.name as rudder, transmission.name as transmission
                    FROM cars
                    LEFT JOIN cars_motor_size ON cars.cars_motor_size_id = cars_motor_size.id
                    LEFT JOIN cars_petrol_type ON cars.petrol_type = cars_petrol_type.id
                    LEFT JOIN rudder ON cars.rudder = rudder.id
                    LEFT JOIN transmission ON cars.transmission = transmission.id
                    WHERE cars.userId = "'.$_SESSION['user_data']['id'].'" ORDER BY id DESC LIMIT :param1, :per_page');
        $this->bind('param1', $param1);
        $this->bind('per_page', $this->per_page);
        $rows = $this->resultSet();
        return $rows;
    }

	public function saveChange(){
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        if(isset($post['name_surname']) && isset($post['email']) && isset($post['phone']) && isset($post['city'])){
            $this->query('UPDATE `u0555183_merged`.users SET nameSurname = :name_surname, mail = :email, phone = :phone, city = :city WHERE id = "'.$_SESSION['user_data']['id'].'"');
            $this->bind('name_surname', $post['name_surname']);
            $this->bind('email', $post['email']);
            $this->bind('phone', $post['phone']);
            $this->bind('city', $post['city']);
            $this->execute();
            Messages::setMsg('Личные данные успешно изменились', 'success');
        }
    }

	public function changePassword(){
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        $this->query('SELECT password FROM `u0555183_merged`.users WHERE id = "'.$_SESSION['user_data']['id'].'"');
        $user = $this->single();
        if(isset($post['new_password']) && isset($post['repeat_password']))
        if($user['password'] == md5($post['current_password'])){
            if(!empty($post['new_password']) && !empty($post['repeat_password'])) {
                if ($post['new_password'] == $post['repeat_password']) {
                    $this->query('UPDATE `u0555183_merged`.users SET password = :password WHERE id = "'.$_SESSION['user_data']['id'].'"');
                    $this->bind('password', md5($post['new_password']));
                    $this->execute();
                    Messages::setMsg('Пароль успешно изменен', 'success');
                } else {
                    Messages::setMsg('Пароли не совпадают', 'error');
                }
            }else {
                Messages::setMsg('Заполните все поля', 'error');
            }
        }else{
            Messages::setMsg('Текущий пароль неверный', 'error');
        }
    }

    public function City(){
        $this->query('SELECT * FROM `u0555183_merged`.city ORDER BY number DESC, city ASC');
        $rows = $this->resultSet();
        return $rows;
    }


    public function deleteProfileImage(){

	    $this->query('SELECT image FROM `u0555183_merged`.users WHERE id = "'.$_SESSION['user_data']['id'].'"');
	    $row = $this->single();

	    $this->query('UPDATE `u0555183_merged`.users SET image = "" WHERE id = "'.$_SESSION['user_data']['id'].'"');
	    $this->execute();
        $image = UPLOAD_SHARED_IMAGE.'user/'.$row['image'];
        unlink($image);
	    die('success');
    }
    
    public function saveImageInDB($image){
        $this->query('UPDATE `u0555183_merged`.users SET image = "'.$image.'" WHERE id = "'.$_SESSION['user_data']['id'].'"');
        $this->execute();
        echo json_encode(SELECT_SHARED_IMAGE.'user/'.$image);
        die();
    }

    public function ImgProcessor(){
            //ვქმნით ფაილის მონაცემებს
            $file = $_FILES['image']['name'];
            $tmp = $_FILES['image']['tmp_name'];
            $filesize = $_FILES['image']['size'];
            $size = floor($filesize / 1024);
            $ext = pathinfo($file, PATHINFO_EXTENSION);
            $ext = strtolower($ext);

            if($ext == "jpg" || $ext == "jpeg"){
                $src = imagecreatefromjpeg($tmp);
            }
            elseif($ext == "png"){
                $src = imagecreatefrompng($tmp);
            }
            else{
                $src = imagecreatefromgif($tmp);
            }

            list($width, $height) = getimagesize($tmp);

            if($width > $height){
                $new_width = 250;
                $new_height = ($height / $width) * $new_width;
            }
            else{
                $new_height = 200;
                $new_width = ($height / $width) * $new_height;
            }

            $resize = imagecreatetruecolor($new_width, $new_height);
            imagecopyresampled($resize, $src, 0,0,0,0, $new_width, $new_height, $width, $height);
            $dir = UPLOAD_SHARED_IMAGE.'user/';
            $filename = time().MD5($_FILES["image"]["name"]).'.'.$ext;
            $full = $dir.$filename;

            imagejpeg($resize,$full,80);
            $imageNames = $filename;
            return $imageNames;
    }
}