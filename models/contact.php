<?php
class ContactModel extends Model{
	public function index(){
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
		
		if(isset($post['send'])){
			
			if(!$post['g-recaptcha-response']){
				Messages::setMsg('Подтвердите что вы не робот !', 'error');
			}
			
			$url = 'https://www.google.com/recaptcha/api/siteverify';
			$key = '6LdLvjwUAAAAACU5rMkFJkKz9GEOIapDT1NOHX_L';
			$request_captcha = $url.'?secret='.$key.'&response='.$post['g-recaptcha-response'].'&remoteip='.$_SERVER['REMOTE_ADDR'];
			
			$data = json_decode(file_get_contents($request_captcha));
			
			if($data->success == false){
				Messages::setMsg('Подтвердите что вы не робот !', 'error');
			}else{
				if(!empty($post['name']) AND !empty($post['mail']) AND !empty($post['subject']) AND !empty($post['text'])){
					$this->mailer($post['name'], $post['mail'], $post['subject'], $post['text']);
					Messages::setMsg('Ваше письмо успешно отправлено. Вам ответят на почту в ближайшее время...', 'success');
				}else{
					Messages::setMsg('Заполните все поля...', 'error');
				}
			}				
		}
	}
	
	//Send Mail
	public function mailer($name, $mail, $userSubject, $text){	
	
		$userInfo = '';
		if(isset($_SESION['user_data'])){
			$userInfo = 'ФИО:'.$_SESSION['user_data']['nameSurname'].'<br/>
						Маил:'.$_SESSION['user_data']['mail'].'<br/>
						Город:'.$_SESSION['user_data']['city'].'<br/>
						Телефон:'.$_SESSION['user_data']['phone'].'<br/>
			
			';
		}
		$to = 'day4shop.ru@gmail.com';
		$message = $userInfo.'<br/>
			Имя: '.$name.'<br/>
			Маил: '.$mail.'<br/>
			Тема: '.$userSubject.'<br/>
			Текст: '.$text.'
		';
		$subject = 'Письмо с контактной формы !';
		
		$headers = "From: Day4shop.ru\r\n";
		$headers .= "Reply-To: Day4shop.ru\r\n";
		$headers .= "CC: Day4shop.ru@gmail.ru\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
		if(mail($to, $subject, $message, $headers)){
			//header('location:'.ROOT_PATH.'');
		}else{
			Messages::setMsg('Неизвестная ошибка...', 'error');
		}
	}
}